package server

import (
	"encoding/json"
	apexlog "github.com/apex/log"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/authoriser"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func Test_proxyPath(t *testing.T) {
	apexlog.SetLevel(apexlog.DebugLevel)
	router := getTestGateway(
		turnstile.Config{
			PublicURLs: []string{"/healthz"},
			Proxies: turnstile.ProxyList{
				&turnstile.HTTPProxy{
					Path:       "/http-bin",
					Target:     "https://httpbin.dmuth.org",
					StripPath:  true,
					Authoriser: nil,
				},
			},
		},
		getAuthorisers()).getRouter()
	// Test that it is responding
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/healthz")).Code)
	request := makeRequest("GET", "/http-bin/headers")
	request.Header.Set("authorisation", "key1")
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code)
	var resultHeaders map[string]string
	assert.NoError(t, json.NewDecoder(response.Result().Body).Decode(&resultHeaders))
	assert.NotEmpty(t, resultHeaders["client-id"])
}

func Test_authProxyHandler_ServeHTTP(t *testing.T) {
	tests := []struct {
		name                       string
		authoriser                 authoriser.Authoriser
		loginProvider              *authoriser.LoginProvider
		useUrl                     string
		useMethod                  string
		expectedProxyHandlerCalled bool
		expectedResponseCode       int
		expectedRedirectLocation   string
	}{
		{
			name:                       "test-pass-single",
			authoriser:                 &mockAuthoriser{respondAuthorised: true},
			expectedProxyHandlerCalled: true,
			expectedResponseCode:       http.StatusOK,
		},
		{
			name:                       "test-block-single",
			authoriser:                 &mockAuthoriser{respondAuthorised: false},
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       http.StatusForbidden,
		},
		{
			name:                       "test-err-single",
			authoriser:                 &mockAuthoriser{respondErr: true, respondAuthorised: true},
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       http.StatusUnauthorized,
		},
		{
			name:                       "test-intercept",
			authoriser:                 &mockAuthoriser{respondErr: true, respondAuthorised: true, interceptRequest: true},
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       501,
		},
		{
			name:       "test-login-provider-redirect",
			authoriser: &mockAuthoriser{respondAuthorised: false},
			loginProvider: &authoriser.LoginProvider{
				Path:    "/authorisation",
				Handler: nil,
			},
			useUrl:                     "/some-target-location?param1=value1",
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       http.StatusTemporaryRedirect,
			expectedRedirectLocation:   "/authorisation/?redirect=" + url.QueryEscape("/some-target-location?param1=value1"),
		},
		{
			name:       "test-login-provider-no-redirect",
			authoriser: &mockAuthoriser{respondAuthorised: false},
			loginProvider: &authoriser.LoginProvider{
				Path:    "/authorisation",
				Handler: nil,
			},
			useUrl:                     "/some-target-location?param1=value1",
			useMethod:                  http.MethodPost,
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       http.StatusForbidden,
		},
		{
			name:       "test-login-provider-serve",
			authoriser: &mockAuthoriser{respondAuthorised: false},
			loginProvider: &authoriser.LoginProvider{
				Path:    "/authorisation",
				Handler: &mockLoginProviderHandler{},
			},
			useUrl:                     "/authorisation/login.html",
			expectedProxyHandlerCalled: false,
			expectedResponseCode:       201,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler := &mockHandler{}
			p := &authProxyHandler{
				proxyHandler: handler,
				proxy: &turnstile.HTTPProxy{
					Authoriser:    tt.authoriser,
					LoginProvider: tt.loginProvider,
				},
				serverHandler401: respondUnauthorized,
				serverHandler403: respondForbidden,
			}
			response := httptest.NewRecorder()
			useUrl := "/"
			if tt.useUrl != "" {
				useUrl = tt.useUrl
			}

			useMethod := "GET"
			if tt.useMethod != "" {
				useMethod = tt.useMethod
			}
			request, _ := http.NewRequest(useMethod, useUrl, nil)
			request.RequestURI = useUrl    // this field is not set when creating new request - should be always set when it is received by the server though
			p.ServeHTTP(response, request) // pass through the proxyHandler handler
			// test that the handler was called
			assert.Equal(t, tt.expectedProxyHandlerCalled, handler.called)
			// test that the response has expected properties
			assert.Equal(t, tt.expectedResponseCode, response.Code)
			// test redirect location
			assert.Equal(t, tt.expectedRedirectLocation, response.Header().Get("Location"))
		})
	}
}

func Test_authProxyHandler_Shutter(t *testing.T) {
	shutterHandlerCalled := false
	handler := &mockHandler{}
	authoriser := &mockAuthoriser{respondAuthorised: false}
	var shutterHanderFunc http.HandlerFunc = func(writer http.ResponseWriter, request *http.Request) {
		shutterHandlerCalled = true
		writer.WriteHeader(400)
	}
	p := &authProxyHandler{
		proxyHandler: handler,
		proxy: &turnstile.HTTPProxy{
			Authoriser: authoriser,
		},
		serverHandler401: respondUnauthorized,
		serverHandler403: respondForbidden,
	}
	response := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)

	// test without a shutter with the authoriser
	p.ServeHTTP(response, request) // pass through the proxyHandler handler
	assert.Equal(t, false, handler.called)
	assert.Equal(t, false, shutterHandlerCalled)
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, true, authoriser.called)
	// test without authoriser
	p.proxy.Authoriser = nil
	handler.called = false
	response = httptest.NewRecorder()
	request, _ = http.NewRequest("GET", "/", nil)
	p.ServeHTTP(response, request) // pass through the proxyHandler handler
	assert.Equal(t, true, handler.called)
	assert.Equal(t, false, shutterHandlerCalled)
	assert.Equal(t, 200, response.Code)
	// test with the shutter - without the authoriser
	handler.called = false
	p.proxy = &turnstile.HTTPProxy{ShutterHandler: shutterHanderFunc}
	response = httptest.NewRecorder()
	request, _ = http.NewRequest("GET", "/", nil)
	p.ServeHTTP(response, request) // pass through the proxyHandler handler
	assert.Equal(t, false, handler.called)
	assert.Equal(t, true, shutterHandlerCalled)
	assert.Equal(t, 400, response.Code)
	// test with shutter and authoriser
	handler.called = false
	authoriser = &mockAuthoriser{respondAuthorised: false}
	p.proxy.Authoriser = &mockAuthoriser{}
	response = httptest.NewRecorder()
	request, _ = http.NewRequest("GET", "/", nil)
	p.ServeHTTP(response, request) // pass through the proxyHandler handler
	assert.Equal(t, false, handler.called)
	assert.Equal(t, true, shutterHandlerCalled)
	assert.Equal(t, false, authoriser.called)
	assert.Equal(t, 400, response.Code)
}

func Test_proxyDomain(t *testing.T) {
	apexlog.SetLevel(apexlog.DebugLevel)
	publicURL := "https://httpbin.dmuth.org" // if httpbin does not work, use the other one: "https://postman-echo.com" | "https://httpbin.org" | "https://httpbin.dmuth.org"
	srv := getTestGateway(
		turnstile.Config{
			PublicURLs:    []string{"/healthz"},
			PublicDomains: []string{"public.domain.com", "anything", "self-protected.domain.com"},
			Proxies: turnstile.ProxyList{
				// test protected domain authentication
				&turnstile.HTTPProxy{Domain: "protected.domain.com", Target: publicURL, Authoriser: nil},
				// test public domain , direct proxyHandler to exact path
				&turnstile.HTTPProxy{Domain: "public.domain.com", Path: "/status-500", Target: "https://postman-echo.com/status/500", StripPath: true, Authoriser: nil},
				// test public domain, with path pointing to another server
				&turnstile.HTTPProxy{Domain: "public.domain.com", Path: "/status/", Target: "https://postman-echo.com", Authoriser: nil},
				// test public domain, no path specified
				&turnstile.HTTPProxy{Domain: "public.domain.com", Target: publicURL, Authoriser: nil},
				// test direct proxyHandler without domain provided (should work for all domains)
				&turnstile.HTTPProxy{Path: "/status-400", Target: "https://postman-echo.com/status/400", StripPath: true, Authoriser: nil},
				// test direct proxyHandler without domain but with shutter page
				&turnstile.HTTPProxy{ID: "shutter-proxy", Path: "/shutter", Target: "https://postman-echo.com/status/400", StripPath: true, Authoriser: nil, ShutterHandler: func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(111)
				}},
				// test route with its own authoriser
				&turnstile.HTTPProxy{Domain: "self-protected.domain.com", Target: publicURL,
					Authoriser: authoriser.NewAPIKeyAuthoriser("authorisation", "out", map[string]string{"proxylevelkey": "validapp"}),
				},
				// proxy with shutter page
				&turnstile.HTTPProxy{Domain: "public.domain.com", Path: "/shutter", Target: "https://postman-echo.com", Authoriser: nil, ShutterHandler: func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(111)
					w.Write([]byte("shutter page"))
				}},
			},
		},
		getAuthorisers())

	router := srv.getRouter()

	// Test domain proxyHandler
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithDomain("GET", "/get", "public.domain.com", nil)).Code)
	assert.Equal(t, 202, sendRequest(router, makeRequestWithDomain("GET", "/status/202", "public.domain.com", nil)).Code)
	assert.Equal(t, 500, sendRequest(router, makeRequestWithDomain("GET", "/status-500", "public.domain.com", nil)).Code)
	assert.Equal(t, 400, sendRequest(router, makeRequestWithAuthHeader("GET", "/status-400", "key1")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", nil)).Code)
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", ptrs.PtrString("key1"))).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", nil)).Code)
	assert.Equal(t, "Status 401 - unauthorized", sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", nil)).Body.String())
	assert.Equal(t, http.StatusForbidden, sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", ptrs.PtrString("key1"))).Code)              // test with key used by top authoriser - wrong key, should fail
	assert.Equal(t, "Status 403 - forbidden", sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", ptrs.PtrString("key1"))).Body.String()) // test with key used by top authoriser - wrong key, should fail
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", ptrs.PtrString("proxylevelkey"))).Code)            // test with the code used by proxy level authoriser,
	assert.Equal(t, 111, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code)                                                                    // test shutter page - should not pass it to the target

	// test shutter for the whole turnstile - pick a few from above
	srv.ShutterHandler(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(112) })
	assert.Equal(t, 112, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", nil)).Code)
	assert.Equal(t, 112, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", ptrs.PtrString("key1"))).Code)
	assert.Equal(t, 112, sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", nil)).Code)
	assert.Equal(t, 112, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code) // test shutter page - should not pass it to the target
	srv.ShutterHandler(nil)                                                                              // reset back and check it works like before
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", ptrs.PtrString("key1"))).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", nil)).Code)
	assert.Equal(t, "Status 401 - unauthorized", sendRequest(router, makeRequestWithDomain("GET", "/get", "self-protected.domain.com", nil)).Body.String())
	assert.Equal(t, 111, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code) // test shutter page - should not pass it to the target
	// test remove individual proxy shutter
	shutterProxy := srv.GetHTTPProxy("shutter-proxy")
	assert.NotNil(t, shutterProxy)
	shutterProxy.ShutterHandler = nil
	assert.Equal(t, 400, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code) // test shutter page - should not pass it to the target
	// -- end of shutter test

	// Test domain proxyHandler with custom route conflict
	// Add custom routes to make sure they are called
	srv.AddRoutes(func(router *mux.Router) {
		router.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("non sense"))
		})
	})
	router = srv.getRouter()
	assert.Equal(t, 111, sendRequest(router, makeRequestWithDomain("GET", "/get", "public.domain.com", nil)).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", nil)).Code)
	assert.Equal(t, 111, sendRequest(router, makeRequestWithDomain("GET", "/get", "protected.domain.com", ptrs.PtrString("key1"))).Code)

}

func Test_proxyShutterPage(t *testing.T) {
	shutterProxy0 := &turnstile.HTTPProxy{ID: "shutter-proxy", Path: "/shutter", Target: "https://postman-echo.com/status/400", StripPath: true, Authoriser: nil, ShutterHandler: func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(111)
	}}
	srv := getTestGateway(
		turnstile.Config{
			PublicURLs:    []string{"/healthz"},
			PublicDomains: []string{"public.domain.com", "anything", "self-protected.domain.com"},
			Proxies: turnstile.ProxyList{
				// test direct proxyHandler without domain but with shutter page
				shutterProxy0,
			},
		},
		getAuthorisers())
	router := srv.getRouter()

	assert.Equal(t, 111, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code) // test shutter page - should not pass it to the target
	// test remove individual proxy shutter
	shutterProxy := srv.GetHTTPProxy("shutter-proxy")
	assert.NotNil(t, shutterProxy)
	shutterProxy.ShutterHandler = nil
	assert.Equal(t, 400, sendRequest(router, makeRequestWithAuthHeader("GET", "/shutter", "key1")).Code) // test shutter page - should not pass it to the target
}

func Test_corsHandler_simple(t *testing.T) {
	testHandler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("success"))
	}))

	type args struct {
		method  string
		origin  string
		headers map[string]string
	}

	tests := []struct {
		name              string
		args              args
		setAllowedOrigins []string
		setAllowedMethods []string
		setAllowedHeaders []string
		expectAllowOrigin string
	}{
		{
			name: "test-no-cors",
			args: args{
				method:  "GET",
				origin:  "https://some.domain.com",
				headers: nil,
			},
			expectAllowOrigin: "",
		},
		{
			name: "test-all-cors",
			args: args{
				method:  "GET",
				origin:  "https://some.domain.com",
				headers: nil,
			},
			setAllowedHeaders: []string{"GET"},
			expectAllowOrigin: "*",
		},
		{
			name: "test-cors-origin-allowed",
			args: args{
				method:  "GET",
				origin:  "https://some.domain.com",
				headers: nil,
			},
			setAllowedOrigins: []string{"https://some.domain.com"},
			expectAllowOrigin: "https://some.domain.com",
		},
		{
			name: "test-cors-origin-rejected",
			args: args{
				method:  "GET",
				origin:  "https://some-other.domain.com",
				headers: nil,
			},
			setAllowedOrigins: []string{"https://some.domain.com"},
			expectAllowOrigin: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var conf *cors.Options
			if len(tt.setAllowedOrigins)+len(tt.setAllowedMethods)+len(tt.setAllowedHeaders) > 0 {
				conf = &cors.Options{
					AllowedOrigins: tt.setAllowedOrigins,
					AllowedMethods: tt.setAllowedMethods,
					AllowedHeaders: tt.setAllowedHeaders,
				}
			}
			corsed := corsHandler(testHandler, conf)
			request, _ := http.NewRequest("GET", "/", nil)
			request.Header.Set("Origin", tt.args.origin)
			response := httptest.NewRecorder()
			corsed.ServeHTTP(response, request)
			assert.Equal(t, tt.expectAllowOrigin, response.Header().Get("Access-Control-Allow-Origin"))

		})
	}
}
