package server

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/pgmtc-lib/turnstile"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

type authProxyHandler struct {
	proxy            *turnstile.HTTPProxy
	proxyHandler     http.Handler
	serverHandler401 http.HandlerFunc
	serverHandler403 http.HandlerFunc
}

func (p *authProxyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	log.Debugf("handling from proxyHandler handler wrapper")
	if p.proxy.ShutterHandler != nil {
		p.proxy.ShutterHandler(w, req)
		return
	}
	if p.proxy.InterceptHandler != nil && p.proxy.InterceptHandler(w, req) {
		// if claimed = true, don't process further
		return
	}
	if p.proxy.Authoriser != nil {
		log.Debugf("passing authorisation request to proxyHandler")
		if intercepted := p.proxy.Authoriser.PreAuthorise(w, req); intercepted {
			return
		}
		if p.proxy.LoginProvider != nil && strings.HasPrefix(req.URL.Path, p.proxy.LoginProvider.NormalizedPath()) {
			log.Debugf("passing login request to login provider")
			rCopy := req.Clone(context.Background())
			rCopy.URL.Path = "/" + strings.TrimPrefix(req.URL.Path, p.proxy.LoginProvider.NormalizedPath()) // get rid of prefix in URL
			p.proxy.LoginProvider.Handler.ServeHTTP(w, rCopy)
			return
		}
		pass, err := p.proxy.Authoriser.Authorise(req)
		if intercepted := p.proxy.Authoriser.PostAuthorise(w, req, pass, err); intercepted {
			return
		}
		if (!pass || err != nil) && p.proxy.LoginProvider != nil && req.Method == http.MethodGet {
			log.Debugf("passing request to redirect to login provider")
			p.proxy.LoginProvider.RedirectToMe(w, req)
			return
		}

		if err != nil {
			log.Debugf("error when authorising: %s", err.Error())
			p.serverHandler401(w, req)
			return
		}
		if !pass {
			log.Debugf("request not authorised")
			p.serverHandler403(w, req)
			return
		}
	}
	// pass the request
	log.Debugf("request has been authorised")
	p.proxyHandler.ServeHTTP(w, req)
}

// newProxyHandlerWrapper wraps generic proxyHandler handler with another handler
func newProxyHandlerWrapper(proxyHandler http.Handler, handler401, handler403 http.HandlerFunc, proxy *turnstile.HTTPProxy) http.Handler {
	// Return proxyHandler handler
	return corsHandler(&authProxyHandler{
		proxyHandler:     proxyHandler, // CORS it
		serverHandler401: handler401,
		serverHandler403: handler403,
		proxy:            proxy,
	}, proxy.CORSConfig)
}

// proxyHandler adds the proxyHandler handler to the router
func proxyHandler(router *mux.Router, config turnstile.Config, proxy *turnstile.HTTPProxy, serverHandler401, serverHandler403 http.HandlerFunc) {
	log.Infof("registering domain proxyHandler %s%s -> %s", proxy.Domain, proxy.Path, proxy.Target)
	origin, _ := url.Parse(proxy.Target)
	proxyHandler := &httputil.ReverseProxy{
		Transport: proxy.Transport,
		Director: func(req *http.Request) {
			req.Header.Add("X-Forwarded-Host", req.Host)
			req.Header.Add("X-Origin-Host", origin.Host)
			req.URL.Scheme = origin.Scheme
			req.URL.Host = origin.Host
			if proxy.StripPath {
				req.URL.Path = strings.TrimPrefix(req.URL.Path, proxy.Path)
			}
			req.URL.Path = origin.Path + req.URL.Path
			log.Debugf("proxying to %s://%s%s from %s", req.URL.Scheme, origin.Host, req.URL.Path, req.RemoteAddr)
			// process request modifiers
			for _, rm := range proxy.RequestModifiers {
				rm.Apply(req)
			}
		},
		ModifyResponse: func(response *http.Response) error {
			if config.CORSOptions != nil || proxy.CORSConfig != nil {
				// wipe out the headers in case they are already incoming
				response.Header.Del("Access-Control-Allow-Headers")
				response.Header.Del("Access-Control-Allow-Methods")
				response.Header.Del("Access-Control-Allow-Origin")
				response.Header.Del("Access-Control-Allow-Credentials")
			}
			return nil
		},
	}
	if proxy.Path == "" {
		proxy.Path = "/"
	}
	if proxy.Domain != "" {
		router.PathPrefix(proxy.Path).Handler(newProxyHandlerWrapper(proxyHandler, serverHandler401, serverHandler403, proxy)).Host(proxy.Domain)
	} else {
		router.PathPrefix(proxy.Path).Handler(newProxyHandlerWrapper(proxyHandler, serverHandler401, serverHandler403, proxy))
	}
}
