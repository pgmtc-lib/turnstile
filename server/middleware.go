package server

import (
	"net/http"
)

// Authorisation middleware - works in the top level
func (g *server) authoriser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if g.disableAuthorizers {
			if next != nil {
				next.ServeHTTP(w, r)
			}
			return
		}
		if g.isPublicURL(r.URL.Path) {
			log.Debugf("authoriser: %s is public URL", r.URL.Path)
			// pass
			if next != nil {
				next.ServeHTTP(w, r)
			}
			return
		}
		if g.isPublicDomain(r.Host) {
			log.Debugf("authoriser: %s is public domain", r.Host)
			// pass
			if next != nil {
				next.ServeHTTP(w, r)
			}
			return
		}
		if g.auth == nil || len(g.auth) == 0 {
			log.Errorf("there is no authoriser set for the server")
			g.handler401(w, r)
			return
		}
		// iterate over authoriser, if any pass, then return
		var anyAuthorised = false
		var anyError = false
		for i, auth := range g.auth {
			if intercepted := auth.PreAuthorise(w, r); intercepted {
				return
			}
			authorised, err := auth.Authorise(r)
			if intercepted := auth.PostAuthorise(w, r, authorised, err); intercepted {
				return
			}
			if err != nil {
				log.Debugf("authoriser: provided authoriser %d returned authorised = %t, error = %s", i, authorised, err.Error())
				anyError = true
				break
			}
			log.Debugf("authoriser: provided authoriser %d returned authorised = %t", i, authorised)
			anyAuthorised = anyAuthorised || authorised
		}
		switch true {
		case anyError:
			log.Debugf("authoriser: returning 401")
			g.handler401(w, r)
		case !anyAuthorised:
			log.Debugf("authoriser: returning 403")
			g.handler403(w, r)
		default:
			log.Debugf("authoriser: passed authorisation")
			if next != nil {
				next.ServeHTTP(w, r)
			}
		}
	})
}

// Logging middleware
func (g *server) logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Debugf("ACCESS %s%s", r.Host, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

// Shutter middleware
func (g *server) shutterMW(next http.Handler) http.Handler {
	if g.shutterHandler != nil {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			g.shutterHandler(w, r)
		})
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

// grpc web middleware - if grpcWeb enabled and incoming request is grpc, handle it, otherwise leave it
func (g *server) grpcWebHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if g.wrappedGrpcServer == nil {
			next.ServeHTTP(w, r)
			return
		}
		if g.wrappedGrpcServer.IsGrpcWebRequest(r) {
			g.wrappedGrpcServer.ServeHTTP(w, r)
			return
		}
		next.ServeHTTP(w, r)
	})
}
