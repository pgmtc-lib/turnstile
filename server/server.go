package server

import (
	"crypto/tls"
	"fmt"
	"github.com/NYTimes/gziphandler"
	apexlog "github.com/apex/log"
	"github.com/gorilla/mux"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/pires/go-proxyproto"
	"github.com/rs/cors"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/authoriser"
	"net"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

var log = apexlog.WithFields(apexlog.Fields{"component": "turnstile"})

// Server returns a generic interface of turnstile server
type Server interface {
	Start() error
	AddRoutes(handler func(router *mux.Router))
	AddAuthoriser(auth authoriser.Authoriser)
	Handle404(handlerFunc http.HandlerFunc)
	Handle401(handlerFunc http.HandlerFunc)
	Handle403(handlerFunc http.HandlerFunc)
	ShutterHandler(handlerFunc http.HandlerFunc)
	AddPublicURL(url string)
	GetHTTPProxy(id string) *turnstile.HTTPProxy
	GetTCPProxy(id string) *turnstile.TCPProxy
}
type server struct {
	conf turnstile.Config
	//certProvider           *turnstile.TLSConfig
	certProvider        turnstile.CertProvider
	auth                []authoriser.Authoriser
	customRoutesHandler func(subrouter *mux.Router)
	handler404          http.HandlerFunc
	handler401          http.HandlerFunc
	handler403          http.HandlerFunc
	shutterHandler      http.HandlerFunc
	routerHandler       http.Handler
	grpcServer          grpcProxyServer
	wrappedGrpcServer   *grpcweb.WrappedGrpcServer
	tcpServer           *tcpProxyServer
	disableAuthorizers  bool
}

// New is a constructor for turnstile
func New(config turnstile.Config, certProvider turnstile.CertProvider) Server {
	return newServer(config, certProvider, false)
}

// NewPublic is a constructor which returns turnstile with disabled top level authorizers
func NewPublic(config turnstile.Config, certProvider turnstile.CertProvider) Server {
	return newServer(config, certProvider, true)
}

// newServer is a generic constructor
func newServer(config turnstile.Config, certProvider turnstile.CertProvider, disableAuthorizers bool) Server {
	gw := &server{
		conf:               config,
		certProvider:       certProvider,
		handler404:         respondNotFound,
		handler401:         respondUnauthorized,
		handler403:         respondForbidden,
		disableAuthorizers: disableAuthorizers,
	}
	if config.GRPCConfig != nil {
		gw.grpcServer = grpcProxyServer{config: *config.GRPCConfig}
	}
	return gw
}

// AddRoutes attaches subrouter to gateway - useful for handling custom login forms, etc
func (g *server) AddRoutes(handler func(router *mux.Router)) {
	g.customRoutesHandler = handler
}

// Start starts the server
func (g *server) Start() (resultErr error) {
	g.routerHandler = g.getRouter()
	if g.conf.CompressResponse {
		g.routerHandler = gziphandler.GzipHandler(g.routerHandler)
	}
	var rootHandler http.Handler = g.routerHandler
	if g.conf.CORSOptions != nil {
		log.Debugf("adding CORS origins: %s", strings.Join(g.conf.CORSOptions.AllowedOrigins, ","))
		log.Debugf("adding CORS methods: %s", strings.Join(g.conf.CORSOptions.AllowedMethods, ","))
		log.Infof("adding CORS headers: %s", strings.Join(g.conf.CORSOptions.AllowedHeaders, ","))
		rootHandler = corsHandler(rootHandler, g.conf.CORSOptions)
	}
	server := &http.Server{Handler: rootHandler}
	var grpcErr, tcpErr, httpErr error
	// start tcp server
	if len(g.conf.Proxies.TCPProxies()) > 0 {
		go func() {
			tcpErr = g.StartTCP()
		}()
	}
	if g.certProvider != nil {
		go func() {
			httpErr = g.startHTTPS(server)
		}()
	} else {
		go func() {
			httpErr = g.startHTTP(server)
		}()
	}
	// start grpc
	time.Sleep(1000 * time.Millisecond)
	if g.conf.GRPCConfig != nil {
		go func() {
			grpcErr = g.startGRPC()
		}()
	}
	time.Sleep(1000 * time.Millisecond)
	if grpcErr != nil {
		resultErr = fmt.Errorf("error when starting grpc server: %s", grpcErr.Error())
		return
	}
	if tcpErr != nil {
		resultErr = fmt.Errorf("error when starting tcp server: %s", tcpErr.Error())
		return
	}
	if httpErr != nil {
		resultErr = fmt.Errorf("error when starting the server: %s", httpErr.Error())
		return
	}
	log.Infof("server has started, listening on %s", server.Addr)
	return
}

// Handle404 sets 404 handler
func (g *server) Handle404(handlerFunc http.HandlerFunc) {
	g.handler404 = handlerFunc
}

// Handle401 sets 401 handler
func (g *server) Handle401(handlerFunc http.HandlerFunc) {
	g.handler401 = handlerFunc
}

// Handle403 sets 403 handler
func (g *server) Handle403(handlerFunc http.HandlerFunc) {
	g.handler403 = handlerFunc
}

// ShutterHandler sets shutter handler - if set, all http responses will be replaced by this
func (g *server) ShutterHandler(handlerFunc http.HandlerFunc) {
	g.shutterHandler = handlerFunc
}

// AddPublicURL adds url to list of public URLs
func (g *server) AddPublicURL(url string) {
	g.conf.PublicURLs = append(g.conf.PublicURLs, url)
}

// GetHTTPProxy finds http proxy by id
func (g *server) GetHTTPProxy(id string) *turnstile.HTTPProxy {
	return g.conf.Proxies.HTTPProxies().Find(id)
}

// GetTCPProxy finds tcp proxy by id
func (g *server) GetTCPProxy(id string) *turnstile.TCPProxy {
	return g.conf.Proxies.TCPProxies().Find(id)
}

// AddAuthoriser adds authoriser to the list of authoriser
func (g *server) AddAuthoriser(auth authoriser.Authoriser) {
	g.auth = append(g.auth, auth)
}

func (g *server) startGRPC() (resultErr error) {
	if err := g.grpcServer.initialize(g.conf.GRPCConfig); err != nil {
		return fmt.Errorf("error when initializing grpc proxy: %s", err.Error())
	}
	// wrap server for grpc web
	if g.conf.GRPCConfig.EnableGrpcWeb {
		log.Debugf("wrapping gRPC proxy server to grpc-web")
		g.wrappedGrpcServer = grpcweb.WrapServer(g.grpcServer.server)
	}
	if !g.conf.GRPCConfig.DisableGrpcListener {
		log.Debugf("starting gRPC proxy server")
		if err := g.grpcServer.Start(); err != nil {
			return fmt.Errorf("error when starting grpc proxy: %s", err.Error())
		}
	}
	return
}

// StartTCP starts tcp proxy
func (g *server) StartTCP() (resultErr error) {
	g.tcpServer = &tcpProxyServer{tcpProxies: g.conf.Proxies.TCPProxies()}
	return g.tcpServer.Start()
}

func (g *server) startHTTP(server *http.Server) (resultErr error) {
	server.Addr = g.conf.ListenerHTTP
	log.Infof("starting server in http mode on %s ...", g.conf.ListenerHTTP)
	//return server.ListenAndServe()
	ppListener, err := createProxyProtocolListener(server.Addr)
	if err != nil {
		return fmt.Errorf("error when creating proxy protocol listener: %w", err)
	}
	return server.Serve(ppListener)
}

func (g *server) startHTTPS(server *http.Server) (resultErr error) {
	if g.certProvider == nil {
		resultErr = fmt.Errorf("certProvider not set")
		return
	}
	server.Addr = g.conf.ListenerHTTPS
	server.TLSConfig = &tls.Config{GetCertificate: g.certProvider.GetCertificate}
	if resultErr = g.certProvider.Initialize(); resultErr != nil {
		return
	}
	log.Infof("starting server in TLS mode on %s ...", g.conf.ListenerHTTPS)
	ppListener, err := createProxyProtocolListener(server.Addr)
	if err != nil {
		return fmt.Errorf("error when creating proxy protocol listener: %w", err)
	}
	return server.ServeTLS(ppListener, "", "")
}

func (g *server) isPublicURL(url string) (result bool) {
	for _, publicURL := range g.conf.PublicURLs {
		if strings.HasPrefix(url, publicURL) {
			return true
		}
	}
	return false
}

func (g *server) isPublicDomain(domain string) (result bool) {
	for _, publicDomain := range g.conf.PublicDomains {
		if match, _ := filepath.Match(publicDomain, domain); match {
			return true
		}
	}
	return false
}

func createProxyProtocolListener(addr string) (result net.Listener, resultErr error) {
	if result, resultErr = net.Listen("tcp", addr); resultErr != nil {
		return
	}
	result = &proxyproto.Listener{
		Listener:          result,
		ReadHeaderTimeout: 200 * time.Millisecond,
	}
	return
}

// corsHandler wraps handler with CORS
func corsHandler(handler http.Handler, corsOptions *cors.Options) http.Handler {
	if corsOptions == nil {
		return handler
	}
	c := cors.New(*corsOptions)
	return c.Handler(handler)
}
