package server

import (
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/turnstile"
	"google.golang.org/grpc"
	"net/http/httptest"
	"testing"
)

func Test_Grpc_AuthViaRouter(t *testing.T) {
	gw := getTestGateway(turnstile.Config{}, nil)
	gw.wrappedGrpcServer = grpcweb.WrapServer(grpc.NewServer())
	router := gw.getRouter()
	// make fake grpc request
	request := httptest.NewRequest("POST", "/", nil)
	request.Header.Add("content-type", "application/grpc-web")
	assert.Equal(t, 401, sendRequest(router, request).Code)

	// make path public
	gw = getTestGateway(turnstile.Config{PublicURLs: []string{"/"}}, nil)
	gw.wrappedGrpcServer = grpcweb.WrapServer(grpc.NewServer())
	router = gw.getRouter()
	// make fake grpc request
	request = httptest.NewRequest("POST", "/", nil)
	request.Header.Add("content-type", "application/grpc-web")
	assert.Equal(t, 200, sendRequest(router, request).Code)
}
