package server

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/mwitkow/grpc-proxy/proxy"
	"gitlab.com/pgmtc-lib/turnstile"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"net"
	"strings"
	"time"
)

type grpcProxyServer struct {
	config     turnstile.GrpcConfig
	creds      credentials.TransportCredentials
	listener   net.Listener
	server     *grpc.Server
	serverOpts []grpc.ServerOption
	started    bool
}

// Start starts the grpc proxy
func (s *grpcProxyServer) Start() (resultErr error) {
	if s.started {
		return fmt.Errorf("error when starting server, server has already started")
	}
	if s.server == nil {
		return fmt.Errorf("error when starting server, grpc server not present")
	}
	if s.listener == nil {
		return fmt.Errorf("error when starting server, listener not present")
	}
	log.Debugf("starting grcpproxy server on %s", s.listener.Addr().String())
	go func() {
		resultErr = s.server.Serve(s.listener)
	}()
	time.Sleep(100 * time.Millisecond)
	if resultErr != nil {
		log.Errorf("error when starting grcpproxy server: %s", resultErr.Error())
	} else {
		log.Infof("grcpproxy server started on %s", s.listener.Addr().String())
		s.started = true
	}
	return
}

// Stop stops the grpc proxy
func (s *grpcProxyServer) Stop() {
	if s.server != nil {
		log.Infof("stopping grcpproxy server")
		s.server.GracefulStop()
	}
}

// initialize creates credentials object and the listener
func (s *grpcProxyServer) initialize(config *turnstile.GrpcConfig) (resultErr error) {
	if config != nil {
		s.config = *config
	}
	if s.config.CertProvider != nil {
		var cert *tls.Certificate
		if resultErr = s.config.CertProvider.Initialize(); resultErr != nil {
			resultErr = fmt.Errorf("error when initializing certProvider: %s", resultErr.Error())
			return
		}
		if cert, resultErr = s.config.CertProvider.GetCertificate(nil); resultErr != nil {
			resultErr = fmt.Errorf("unable to get certificate: %s", resultErr.Error())
			return
		} // this will not work for letsencrypt
		s.creds = credentials.NewServerTLSFromCert(cert)
	}
	//if s.config.ServerKey != "" && s.config.ServerCert != "" {
	//	if s.creds, resultErr = credentials.NewServerTLSFromFile(s.config.ServerCert, s.config.ServerKey); resultErr != nil {
	//		return
	//	}
	//}
	if s.listener, resultErr = createProxyProtocolListener(s.config.Listener); resultErr != nil {
		return
	}
	// initialize server opts
	s.serverOpts = []grpc.ServerOption{}
	s.serverOpts = append(s.serverOpts, grpc.CustomCodec(proxy.Codec()))
	s.serverOpts = append(s.serverOpts, grpc.UnknownServiceHandler(proxy.TransparentHandler(s.proxyDirector)))
	if s.creds != nil {
		log.Debug("adding credentials")
		s.serverOpts = append(s.serverOpts, grpc.Creds(s.creds))
	}
	for proxySource, proxyTarget := range s.config.Targets {
		log.Infof("registering gRPC proxy target %s -> %s", proxySource, proxyTarget)
	}
	log.Debug("creating grpc NewServer")
	s.server = grpc.NewServer(s.serverOpts...)
	return
}

// proxyDirector is where it is decided where the server should proxy to
func (s *grpcProxyServer) proxyDirector(ctx context.Context, fullMethodName string) (context.Context, *grpc.ClientConn, error) {
	// Make sure we never forward internal services.
	log.Debugf("grpc call: %s", fullMethodName)
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, nil, status.Errorf(codes.Unimplemented, "invalid metadata")
	}
	log.Debugf("request metadata: %s", md)

	// get target
	target, err := getTarget(md, s.config.Targets)
	if err != nil {
		return nil, nil, status.Error(codes.Unavailable, err.Error())
	}
	// Copy the inbound metadata explicitly.
	outCtx := metadata.NewOutgoingContext(ctx, md.Copy())
	// Dial the target
	// conn, err := grpc.DialContext(ctx, target, grpc.WithCodec(proxy.Codec()), grpc.WithInsecure())
	conn, err := grpc.DialContext(ctx, target, grpc.WithDefaultCallOptions(grpc.CustomCodecCallOption{Codec: proxy.Codec()}), grpc.WithInsecure())
	return outCtx, conn, err
}

func getTarget(md metadata.MD, targets map[string]string) (target string, resultErr error) {
	var domain string
	var ok bool
	if domain, resultErr = parseMetaDomain(md); resultErr != nil {
		return
	}
	if target, ok = targets[domain]; !ok {
		resultErr = fmt.Errorf("%s has not been found among targets", domain)
	}
	return
}

func parseMetaDomain(md metadata.MD) (result string, resultErr error) {
	authorities, exists := md[":authority"]
	if !exists {
		resultErr = fmt.Errorf("no authority metadata")
		return
	}
	if len(authorities) != 1 {
		resultErr = fmt.Errorf("unexpected length %d: %s", len(authorities), authorities)
		return
	}
	domainPort := authorities[0]
	domainPortSplit := strings.Split(domainPort, ":")
	if len(domainPortSplit) > 2 {
		resultErr = fmt.Errorf("unexpected domain split length %d: %s", len(domainPortSplit), domainPort)
		return
	}
	result = domainPortSplit[0]
	return
}
