package server

import (
	"fmt"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/authoriser"
	"net/http"
	"net/http/httptest"
)

type mockLoginProviderHandler struct {
	gotRequestURL string
}

func (m *mockLoginProviderHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(201)
	m.gotRequestURL = r.URL.String()
	_, _ = w.Write([]byte("login provider response"))
}

type mockAuthoriser struct {
	interceptRequest  bool
	respondAuthorised bool
	respondErr        bool
	called            bool
}

func (m *mockAuthoriser) PreAuthorise(w http.ResponseWriter, r *http.Request) (claimed bool) {
	if m.interceptRequest {
		w.WriteHeader(501)
		_, _ = w.Write([]byte("intercepted response"))
		return true
	}
	return false
}

func (m *mockAuthoriser) Authorise(r *http.Request) (authorised bool, resultErr error) {
	m.called = true
	var err error
	if m.respondErr {
		err = fmt.Errorf("artificial error")
	}
	return m.respondAuthorised, err
}

func (m *mockAuthoriser) PostAuthorise(w http.ResponseWriter, r *http.Request, authorised bool, authoriseError error) (claimed bool) {
	return false
}

type mockHandler struct{ called bool }

func (m *mockHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	m.called = true
}

func getTestGateway(config turnstile.Config, authorisers []authoriser.Authoriser) *server {
	gw := newServer(config, nil, false).(*server)
	gw.handler401 = respondUnauthorized
	gw.handler403 = respondForbidden
	gw.handler404 = respondNotFound
	if authorisers != nil {
		gw.auth = authorisers
	}
	return gw
}

func getAuthorisers() []authoriser.Authoriser {
	auth := authoriser.NewAPIKeyAuthoriser("authorisation", "client-id", map[string]string{"key1": "uid1"})
	return []authoriser.Authoriser{auth}
}

func getProxies() turnstile.ProxyList {
	return turnstile.ProxyList{
		&turnstile.HTTPProxy{Path: "/http-bin", Target: "https://httpbin.dmuth.org", StripPath: true, Authoriser: nil},
	}
}

func makeRequest(method, path string) *http.Request {
	request, _ := http.NewRequest(method, path, nil)
	return request
}

func makeRequestWithAuthHeader(method, path string, authHeaderValue string) *http.Request {
	request := makeRequest(method, path)
	request.Header.Set("authorisation", authHeaderValue)
	return request
}

func makeRequestWithDomain(method, path, domain string, authHeaderValue *string) *http.Request {
	request := makeRequest(method, path)
	if authHeaderValue != nil {
		request.Header.Set("authorisation", *authHeaderValue)
	}
	request.Host = domain
	return request
}

func sendRequest(handler http.Handler, request *http.Request) (response *httptest.ResponseRecorder) {
	response = httptest.NewRecorder()
	handler.ServeHTTP(response, request)
	//fmt.Printf("%s ... %d - %s\n", request.URL, response.Code, response.Body.String())
	return
}
