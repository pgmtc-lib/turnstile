package server

import "net/http"

// AddBasicAuth adds basic auth to the request
type AddBasicAuth struct {
	User     string
	Password string
}

func (m AddBasicAuth) Apply(req *http.Request) {
	log.Debugf("adding basic auth for %s", m.User)
	req.SetBasicAuth(m.User, m.Password)
}

// AddUserAgent adds user agent to the request
type AddUserAgent struct {
	UserAgent string
}

func (m AddUserAgent) Apply(req *http.Request) {
	req.Header.Set("User-Agent", m.UserAgent)
}
