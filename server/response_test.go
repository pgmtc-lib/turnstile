package server

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_respondNotFound(t *testing.T) {
	req := makeRequest("GET", "/whatever")
	rr := httptest.NewRecorder()
	respondNotFound(rr, req)
	assert.Equal(t, http.StatusNotFound, rr.Code)
}

func Test_respondUnauthorized(t *testing.T) {
	req := makeRequest("GET", "/whatever")
	rr := httptest.NewRecorder()
	respondUnauthorized(rr, req)
	assert.Equal(t, http.StatusUnauthorized, rr.Code)
}

func Test_respondForbidden(t *testing.T) {
	req := makeRequest("GET", "/whatever")
	rr := httptest.NewRecorder()
	respondForbidden(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}
