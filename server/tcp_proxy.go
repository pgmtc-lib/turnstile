package server

import (
	"fmt"
	"gitlab.com/pgmtc-lib/turnstile"
	"io"
	"net"
	"time"
)

type tcpProxyServer struct {
	tcpProxies []*turnstile.TCPProxy
	started    bool
	listeners  []net.Listener
}

// Start starts the tcp proxy
func (s *tcpProxyServer) Start() (resultErr error) {
	if s.started {
		return fmt.Errorf("error when starting server, server has already started")
	}
	log.Debugf("starting tcp proxies")
	for _, tcpProxy := range s.tcpProxies {
		listener, err := startProxy(tcpProxy)
		if err != nil {
			resultErr = fmt.Errorf("cannot start tcp proxy %s: %s", tcpProxy.Listener, err.Error())
			return
		}
		s.listeners = append(s.listeners, listener)
	}
	log.Infof("all TCP proxies have started")
	s.started = true
	return
}

// Stop stops the grpc proxy
func (s *tcpProxyServer) Stop() {
	log.Debugf("stopping TCP listeners")
	for _, listener := range s.listeners {
		_ = listener.Close()
	}
	log.Infof("TCP listeners stopped")
}

func startProxy(tcpProxyConfig *turnstile.TCPProxy) (result net.Listener, resultErr error) {
	log.Infof("starting TCP proxy on %s -> %s", tcpProxyConfig.Listener, tcpProxyConfig.Target)
	ppListener, err := createProxyProtocolListener(tcpProxyConfig.Listener)
	log.Debugf("proxyProtocol for %s -> %s has been requested", tcpProxyConfig.Listener, tcpProxyConfig.Target)

	if err != nil {
		resultErr = fmt.Errorf("cannot listen: %s", err.Error())
		return
	}
	go func() {
		for {
			conn, err := ppListener.Accept()
			if conn == nil {
				log.Errorf("accept failed: %v", err)
				continue
			}
			if tcpProxyConfig.Authoriser != nil && !tcpProxyConfig.Authoriser(conn) {
				log.Debugf("rejecting connection from %s", conn.RemoteAddr())
				_ = conn.Close()
				continue
			}
			log.Debugf("accepting connection from %s", conn.RemoteAddr())
			go forward(conn, tcpProxyConfig.Target)
		}
	}()

	result = ppListener
	return
}

func forward(local net.Conn, remoteAddr string) {
	log.Debugf("connection for %s from %s create", local.LocalAddr(), local.RemoteAddr())
	remote, err := net.DialTimeout("tcp", remoteAddr, 3*time.Second)
	if remote == nil {
		log.Errorf("remote dial failed: %v", err)
		local.Close()
		return
	}
	done := make(chan struct{})
	go func() {
		defer local.Close()
		defer remote.Close()
		_, _ = io.Copy(remote, local)
		done <- struct{}{}
	}()

	go func() {
		defer local.Close()
		defer remote.Close()
		_, _ = io.Copy(local, remote)
		done <- struct{}{}
	}()
	<-done
	<-done
	log.Debugf("connection for %s from %s closed", local.LocalAddr(), local.RemoteAddr())
}
