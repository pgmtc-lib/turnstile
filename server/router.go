package server

import (
	"github.com/gorilla/mux"
	"gitlab.com/pgmtc-lib/commons/htutils"
	"net/http"
)

func (g *server) getRouter() *mux.Router {
	router := mux.NewRouter()
	router.Use(htutils.PanicRecoveryMiddleWare(log.Errorf), g.shutterMW, g.logger, g.authoriser)
	if g.conf.GRPCConfig != nil && g.conf.GRPCConfig.EnableGrpcWeb {
		router.Use(g.grpcWebHandler)
	}
	if g.customRoutesHandler != nil {
		g.customRoutesHandler(router.PathPrefix("/").Subrouter())
	}
	for _, proxyDef := range g.conf.Proxies.HTTPProxies() {
		proxyHandler(router, g.conf, proxyDef, g.handler401, g.handler403)
	}
	if g.conf.Handler != nil {
		router.PathPrefix("/").Handler(g.conf.Handler)
	} else {
		router.HandleFunc("/", g.rootHandler)
		router.HandleFunc("/healthz", g.healthzHandler)
	}
	router.NotFoundHandler = router.NewRoute().HandlerFunc(g.handler404).GetHandler()
	return router
}

func (g *server) rootHandler(w http.ResponseWriter, req *http.Request) {
	respondOK(w, req)
}

func (g *server) healthzHandler(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Status 200 - healthy"))
}
