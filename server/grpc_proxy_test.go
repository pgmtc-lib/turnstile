package server

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/server/certprovider"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"net"
	"testing"
)

func Test_grpcProxyServer_initialize(t *testing.T) {
	// test bare, without autnentication
	srv := grpcProxyServer{}
	err := srv.initialize(nil)
	assert.NoError(t, err)
	assert.Len(t, srv.serverOpts, 2)
	assert.NotNil(t, srv.server)

	// test invalid listener
	config := turnstile.GrpcConfig{Listener: ":CRAP"}
	srv = grpcProxyServer{}
	err = srv.initialize(&config)
	assert.Equal(t, config, srv.config)
	assert.Error(t, err)
	assert.Nil(t, srv.server)

	// test valid initialization
	config = turnstile.GrpcConfig{
		CertProvider: certprovider.GenerateSelfSigned("turnstile", []string{}),
		Listener:     ":0",
		Targets: map[string]string{
			"source1": "target1:3009",
			"source2": "target2:3009",
		},
	}
	srv = grpcProxyServer{}
	err = srv.initialize(&config)
	assert.NoError(t, err)
	assert.Len(t, srv.serverOpts, 3)
	assert.Equal(t, config, srv.config)
	assert.NotNil(t, srv.server)
}

func Test_grpcProxyServer_StartStop(t *testing.T) {
	var err error
	srv := grpcProxyServer{}
	err = srv.Start()
	assert.EqualError(t, err, "error when starting server, grpc server not present")

	// test missing listener
	srv = grpcProxyServer{}
	srv.server = grpc.NewServer()
	err = srv.Start()
	assert.EqualError(t, err, "error when starting server, listener not present")

	// test with invalid listener
	srv = grpcProxyServer{}
	srv.server = grpc.NewServer()
	srv.listener, err = net.Listen("tcp", ":7777")
	assert.NoError(t, err)
	srv.listener.Close()
	err = srv.Start()
	assert.EqualError(t, err, "accept tcp [::]:7777: use of closed network connection")
	// test with valid listener
	srv = grpcProxyServer{}
	srv.server = grpc.NewServer()
	srv.listener, err = net.Listen("tcp", ":7777")
	assert.NoError(t, err)
	err = srv.Start()
	assert.NoError(t, err)

	// test already started message
	err = srv.Start()
	assert.EqualError(t, err, "error when starting server, server has already started")

	// test stop
	srv.Stop()
	err = srv.listener.Close() // test if I close it manually, it faiils
	assert.EqualError(t, err, "close tcp [::]:7777: use of closed network connection")
}

func Test_grpcProxyServer_proxyDirector(t *testing.T) {
	srv := grpcProxyServer{}
	ctx := context.Background()
	methodName := "com.example.Method"
	// test with invalid incoming request
	outCtx, clientConn, err := srv.proxyDirector(ctx, methodName)
	assert.EqualError(t, err, "rpc error: code = Unimplemented desc = invalid metadata")
	assert.Nil(t, outCtx)
	assert.Nil(t, clientConn)

	// test with missing target
	testMetadata := metadata.MD{
		":authority": []string{"localhost:777"},
	}
	ctx = metadata.NewIncomingContext(ctx, testMetadata)
	outCtx, clientConn, err = srv.proxyDirector(ctx, methodName)
	assert.EqualError(t, err, "rpc error: code = Unavailable desc = localhost has not been found among targets")
	assert.Nil(t, outCtx)
	assert.Nil(t, clientConn)

	// test with valid targets
	srv.config.Targets = map[string]string{
		"localhost": "some-target:3009",
	}
	ctx = metadata.NewIncomingContext(ctx, testMetadata)
	outCtx, clientConn, err = srv.proxyDirector(ctx, methodName)
	assert.NoError(t, err)
	assert.NotNil(t, outCtx)
	assert.NotNil(t, clientConn)
	assert.Equal(t, "some-target:3009", clientConn.Target())
	inMetadata, inOk := metadata.FromIncomingContext(ctx)
	outMetadata, outOk := metadata.FromOutgoingContext(outCtx)
	assert.True(t, inOk)
	assert.True(t, outOk)
	assert.Equal(t, inMetadata, outMetadata)
}

func Test_parseMetaDomain(t *testing.T) {
	tests := []struct {
		name       string
		in         metadata.MD
		wantResult string
		wantErr    bool
	}{
		{
			name:       "test-success",
			in:         map[string][]string{":authority": {"localhost:123"}},
			wantResult: "localhost",
			wantErr:    false,
		},
		{
			name:       "test-success",
			in:         map[string][]string{":authority": {"localhost"}},
			wantResult: "localhost",
			wantErr:    false,
		},
		{
			name:    "test-wrongformat",
			in:      map[string][]string{":authority": {"localhost:123:789"}},
			wantErr: true,
		},
		{
			name:    "test-toomany",
			in:      map[string][]string{":authority": {"localhost:123", "localhost:456"}},
			wantErr: true,
		},
		{
			name:    "test-missing",
			in:      map[string][]string{":missing": {"localhost:123"}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := parseMetaDomain(tt.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseMetaDomain() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotResult != tt.wantResult {
				t.Errorf("parseMetaDomain() gotResult = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func Test_getTarget(t *testing.T) {
	type args struct {
		md      metadata.MD
		targets map[string]string
	}
	tests := []struct {
		name       string
		args       args
		wantTarget string
		wantErr    bool
	}{
		{
			name: "test-success",
			args: args{
				md:      map[string][]string{":authority": {"localhost:123"}},
				targets: map[string]string{"localhost": "target-server:1234"},
			},
			wantTarget: "target-server:1234",
			wantErr:    false,
		},
		{
			name: "test-missing-target",
			args: args{
				md:      map[string][]string{":authority": {"nonexisting:123"}},
				targets: map[string]string{"localhost": "target-server:1234"},
			},
			wantErr: true,
		},
		{
			name: "test-domain-wrong",
			args: args{
				md:      map[string][]string{":authority": {"localhost:123:456"}},
				targets: map[string]string{"localhost": "target-server:1234"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTarget, err := getTarget(tt.args.md, tt.args.targets)
			if (err != nil) != tt.wantErr {
				t.Errorf("getTarget() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotTarget != tt.wantTarget {
				t.Errorf("getTarget() gotTarget = %v, want %v", gotTarget, tt.wantTarget)
			}
		})
	}
}
