package certprovider

import (
	"crypto/tls"
	"fmt"
	apexlog "github.com/apex/log"
	"gitlab.com/pgmtc-lib/turnstile"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"time"
)

type letsEncrypt struct {
	certManager *autocert.Manager
}

// LetsEncryptCertProvider returns cert provider for let's encrypt
func LetsEncryptCertProvider(certManager *autocert.Manager) turnstile.CertProvider {
	return &letsEncrypt{certManager: certManager}
}

func (l letsEncrypt) Initialize() (resultErr error) {
	apexlog.WithField("component", "turnstile/server/certprovider").Debugf("initializing lets encrypt cert provider")
	apexlog.WithField("component", "turnstile/server/certprovider/letsencrypt").Infof("Starting http ACME challenge server")
	var errChan = make(chan error)
	go func() {
		errChan <- http.ListenAndServe(":0", l.certManager.HTTPHandler(nil))
	}()
	// wait for error with timeout
	for {
		select {
		case resultErr = <-errChan:
			return
		case <-time.After(500 * time.Millisecond):
			return
		}
	}
}
func (l letsEncrypt) GetCertificate(info *tls.ClientHelloInfo) (*tls.Certificate, error) {
	if info == nil {
		return nil, fmt.Errorf("info object must be provide for let's encrypt provider")
	}
	return l.certManager.GetCertificate(info)
}
