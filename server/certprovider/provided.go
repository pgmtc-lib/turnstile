package certprovider

import (
	"crypto/tls"
	"github.com/apex/log"
	"gitlab.com/pgmtc-lib/turnstile"
)

type provide struct {
	certPem []byte          // this is then generated
	keyPem  []byte          // this is then generated
	cert    tls.Certificate // this is returned
}

// Provide returns cert provider for let's encrypt
func Provide(certPem []byte, keyPem []byte) turnstile.CertProvider {
	return &provide{
		certPem: certPem,
		keyPem:  keyPem,
	}
}

func (l *provide) Initialize() (resultErr error) {
	log.WithField("component", "turnstile/server/certprovider").Debugf("initializing provide certificate provider")
	l.cert, resultErr = tls.X509KeyPair(l.certPem, l.keyPem)
	return
}

func (l *provide) GetCertificate(info *tls.ClientHelloInfo) (result *tls.Certificate, resultErr error) {
	return &l.cert, resultErr
}
