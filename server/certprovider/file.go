package certprovider

import (
	"crypto/tls"
	"github.com/apex/log"
	"gitlab.com/pgmtc-lib/turnstile"
	"io/ioutil"
)

type files struct {
	certPemFile string
	keyPemFile  string
	provide
}

// FromFiles creates cert provider which loads certificates from files
func FromFiles(certPemFile, keyPemFile string) turnstile.CertProvider {
	return &files{
		certPemFile: certPemFile,
		keyPemFile:  keyPemFile,
	}
}

func (l *files) Initialize() (resultErr error) {
	log.WithField("component", "turnstile/server/certprovider").Debugf("initializing file certificate provider")
	var certBytes, keyBytes []byte
	if certBytes, resultErr = ioutil.ReadFile(l.certPemFile); resultErr != nil {
		return
	}
	if keyBytes, resultErr = ioutil.ReadFile(l.keyPemFile); resultErr != nil {
		return
	}

	// create 'provide' provider
	l.provide = provide{certPem: certBytes, keyPem: keyBytes}
	return l.provide.Initialize()
}

func (l *files) GetCertificate(info *tls.ClientHelloInfo) (result *tls.Certificate, resultErr error) {
	return &l.cert, resultErr
}
