package certprovider

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"github.com/apex/log"
	"gitlab.com/pgmtc-lib/turnstile"
	"math/big"
	"net"
	"time"
)

type generateSelfSigned struct {
	orgName string // this is used for generating certificate
	hosts   []string
	certPem []byte // this is then generated
	keyPem  []byte // this is then generated
}

// GenerateSelfSigned returns cert provider for let's encrypt
func GenerateSelfSigned(orgName string, hosts []string) turnstile.CertProvider {
	return &generateSelfSigned{orgName: orgName, hosts: hosts}
}

func (l *generateSelfSigned) Initialize() (resultErr error) {
	log.WithField("component", "turnstile/server/certprovider").Debugf("initializing self signed certificate provider")
	return l.generateCertificate()
}

func (l *generateSelfSigned) GetCertificate(info *tls.ClientHelloInfo) (result *tls.Certificate, resultErr error) {
	var cert tls.Certificate
	if len(l.certPem) == 0 || len(l.keyPem) == 0 {
		if resultErr = l.generateCertificate(); resultErr != nil {
			return
		}
	}
	cert, resultErr = tls.X509KeyPair(l.certPem, l.keyPem)
	result = &cert
	return
}

func (l *generateSelfSigned) generateCertificate() (resultErr error) {
	log.Debugf("generating self signed certificate for organization %s", l.orgName)
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return fmt.Errorf("error when genearating certificate: %s", err.Error())
	}
	notBefore := time.Now()
	notAfter := notBefore.Add(10 * 365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		resultErr = fmt.Errorf("failed to generate serial number: %s", err.Error())
		return
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{l.orgName},
		},
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	for _, h := range l.hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		resultErr = fmt.Errorf("failed to create certificate: %s", err.Error())
		return
	}

	certOut := &bytes.Buffer{}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	keyOut := &bytes.Buffer{}
	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		resultErr = fmt.Errorf("unable to marshal private key: %s", err.Error())
		return
	}
	pem.Encode(keyOut, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes})
	l.certPem = certOut.Bytes()
	l.keyPem = keyOut.Bytes()
	log.Infof("self signed certificate for organization %s successfully generated", l.orgName)
	return
}
