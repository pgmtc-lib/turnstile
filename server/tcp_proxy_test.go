package server

import (
	"bytes"
	"github.com/pires/go-proxyproto"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/turnstile"
	"net"
	"testing"
	"time"
)

func Test_tcpProxyServer_StartStop(t *testing.T) {
	go func() {
		time.Sleep(1 * time.Second)
		log.Fatalf("Test_tcpProxyServer_StartStop timeout")
	}()
	srv := tcpProxyServer{
		tcpProxies: []*turnstile.TCPProxy{
			{Listener: ":3000", Target: ":3001"},
			{Listener: ":3010", Target: ":3011"},
			{Listener: ":3020", Target: ":3011", Authoriser: func(conn net.Conn) (authorized bool) {
				return false
			}},
		},
		started:   false,
		listeners: nil,
	}
	dataChan1 := make(chan []byte)
	dataChan2 := make(chan []byte)
	go startTestTCPListener(t, ":3001", dataChan1)
	go startTestTCPListener(t, ":3011", dataChan2)
	assert.False(t, srv.started)
	assert.NoError(t, srv.Start())

	assert.True(t, srv.started)

	// send to non-proxied
	got, err := sendTestTCPData("localhost:3001", "non-proxied", false)
	assert.NoError(t, err)
	assert.Equal(t, "non-proxied response", got)
	assert.Equal(t, "non-proxied", string(<-dataChan1))

	// send to proxied
	got, err = sendTestTCPData("localhost:3000", "proxied", false)
	assert.NoError(t, err)
	assert.Equal(t, "proxied response", got)
	assert.Equal(t, "proxied", string(<-dataChan1))

	// send to non-proxied
	got, err = sendTestTCPData("localhost:3011", "non-proxied", false)
	assert.NoError(t, err)
	assert.Equal(t, "non-proxied response", got)
	assert.Equal(t, "non-proxied", string(<-dataChan2))

	// send to proxied
	got, err = sendTestTCPData("localhost:3010", "proxied", true)
	assert.NoError(t, err)
	assert.Equal(t, "proxied response", got)
	assert.Equal(t, "proxied", string(<-dataChan2))

	// send to proxied - with rejecting authoriser
	got, err = sendTestTCPData("localhost:3020", "proxied", false)
	assert.EqualError(t, err, "EOF")
	assert.Equal(t, "", got)

	// stop the proxy
	srv.Stop()
	// send to non-proxied - 1
	got, err = sendTestTCPData("localhost:3001", "non-proxied", false)
	assert.NoError(t, err)
	assert.Equal(t, "non-proxied response", got)
	assert.Equal(t, "non-proxied", string(<-dataChan1))
	// send to non-proxied - 2
	got, err = sendTestTCPData("localhost:3011", "non-proxied", false)
	assert.NoError(t, err)
	assert.Equal(t, "non-proxied response", got)
	assert.Equal(t, "non-proxied", string(<-dataChan2))

	// send to proxied - 1
	_, err = sendTestTCPData("localhost:3000", "proxied", false)
	assert.Error(t, err)

	// send to proxied - 2
	_, err = sendTestTCPData("localhost:3010", "proxied", false)
	assert.Error(t, err)
}

func sendTestTCPData(addr string, data string, withProxyProtocol bool) (result string, resultErr error) {
	var conn net.Conn
	if conn, resultErr = net.Dial("tcp", addr); resultErr != nil {
		return
	}
	if withProxyProtocol {
		header := &proxyproto.Header{
			Version:           1,
			Command:           proxyproto.PROXY,
			TransportProtocol: proxyproto.TCPv4,
			SourceAddr: &net.TCPAddr{
				IP:   net.ParseIP("10.1.1.1"),
				Port: 1000,
			},
			DestinationAddr: &net.TCPAddr{
				IP:   net.ParseIP("20.2.2.2"),
				Port: 2000,
			},
		}
		// After the connection was created write the proxy headers first
		_, err := header.WriteTo(conn)
		if err != nil {
			panic(err)
		}
	}
	if _, resultErr = conn.Write([]byte(data)); resultErr != nil {
		return
	}
	buf := make([]byte, 1024)
	if _, resultErr = conn.Read(buf); resultErr != nil {
		return
	}
	result = string(bytes.Trim(buf, "\x00"))
	return
}

func startTestTCPListener(t *testing.T, listener string, dataChan chan []byte) {
	l, err := net.Listen("tcp", listener)
	if err != nil {
		return
		//t.Fatal(err)
	}
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			return
		}
		defer conn.Close()

		buf := make([]byte, 1024)
		_, err = conn.Read(buf)
		if err != nil {
			//t.Fatal(err)
			return
		}
		in := bytes.Trim(buf, "\x00")
		conn.Write([]byte(string(in) + " response"))
		dataChan <- in
	}
}
