package server

import "net/http"

func respondOK(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("Status 200 - OK"))
}
func respondNotFound(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(404)
	_, _ = w.Write([]byte("Status 404 - not found"))
}

func respondUnauthorized(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusUnauthorized)
	_, _ = w.Write([]byte("Status 401 - unauthorized"))
}

func respondForbidden(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusForbidden)
	_, _ = w.Write([]byte("Status 403 - forbidden"))
}
