package server

import (
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/server/certprovider"
	"net/http"
	"reflect"
	"testing"
	"time"
)

func Test_NewPublic(t *testing.T) {
	// test non-public
	got := New(turnstile.Config{}, nil)
	assert.False(t, got.(*server).disableAuthorizers)
	// test public
	got = NewPublic(turnstile.Config{}, nil)
	assert.True(t, got.(*server).disableAuthorizers)
}
func Test_new(t *testing.T) {
	// Test without TLS
	var config = turnstile.Config{}
	var certProvider turnstile.CertProvider

	expected := &server{
		conf:         config,
		certProvider: certProvider,
		handler404:   respondNotFound,
		handler401:   respondUnauthorized,
		handler403:   respondForbidden,
	}
	got := newServer(config, certProvider, true).(*server)
	assert.Equal(t, expected.conf, got.conf)
	assert.Equal(t, expected.certProvider, got.certProvider)
	assert.Equal(t, reflect.ValueOf(expected.handler401), reflect.ValueOf(got.handler401))
	assert.Equal(t, reflect.ValueOf(expected.handler403), reflect.ValueOf(got.handler403))
	assert.Equal(t, reflect.ValueOf(expected.handler404), reflect.ValueOf(got.handler404))
	assert.Equal(t, true, got.disableAuthorizers)

	// Test with Configs
	config = turnstile.Config{
		GRPCConfig: &turnstile.GrpcConfig{},
	}
	certProvider = certprovider.GenerateSelfSigned("turnstile", []string{})

	expected = &server{
		conf:         config,
		certProvider: certProvider,
		handler404:   respondNotFound,
		handler401:   respondUnauthorized,
		handler403:   respondForbidden,
	}
	got = newServer(config, certProvider, false).(*server)
	assert.Equal(t, expected.conf, got.conf)
	assert.Equal(t, expected.certProvider, got.certProvider)
	assert.Equal(t, expected.certProvider, got.certProvider)
	assert.Equal(t, reflect.ValueOf(expected.handler401), reflect.ValueOf(got.handler401))
	assert.Equal(t, reflect.ValueOf(expected.handler403), reflect.ValueOf(got.handler403))
	assert.Equal(t, reflect.ValueOf(expected.handler404), reflect.ValueOf(got.handler404))
	assert.False(t, got.disableAuthorizers)
}

func Test_server(t *testing.T) {
	// test start http
	gw := New(turnstile.Config{}, nil)
	assert.NoError(t, gw.Start())
	// TODO:
	// 1 - test that HTTP is opened and listening
	// 2 - test that request to / returns unauthorised

	// test start https
	gw = New(turnstile.Config{}, certprovider.GenerateSelfSigned("turnstile", []string{}))
	assert.NoError(t, gw.Start())
	// TODO:
	// 1 - test that HTTPS is opened and listening
	// 2 - test that request to / returns unauthorised
	// 3 - test that HTTP is open and listening
	// 4 - test that it returns something

	time.Sleep(1 * time.Second)
}

/*	This test is for server, tests that authoriser is protecting all URLs with exception of public URLs */
func Test_server_bareMinimum(t *testing.T) {
	// test without any authoriser, no public URLs - should reject
	router := getTestGateway(turnstile.Config{Proxies: getProxies()}, nil).getRouter()
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/non-existing/file")).Code)

	// test with some authoriser - no public URLs, no authorised request, should reject
	router = getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers()).getRouter()
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/non-existing/file")).Code)

	// test with all public - no authoriser - should pass
	router = getTestGateway(turnstile.Config{Proxies: getProxies(), PublicURLs: []string{"/"}}, nil).getRouter()
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	assert.Equal(t, http.StatusNotFound, sendRequest(router, makeRequest("GET", "/non-existing/file")).Code)

	// test with all public - with authoriser - should pass
	router = getTestGateway(turnstile.Config{Proxies: getProxies(), PublicURLs: []string{"/"}}, getAuthorisers()).getRouter()
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	assert.Equal(t, http.StatusNotFound, sendRequest(router, makeRequest("GET", "/non-existing/file")).Code)
}

/* This test is for server, tests that if the request has authorisation in it, it passes to the right places */
func Test_server_authorisedRequests(t *testing.T) {
	// Test that it is blocking when no authorised request provided
	router := getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers()).getRouter()
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/non-existing/file")).Code)

	// Create request with authorisation data - wrong user
	assert.Equal(t, http.StatusForbidden, sendRequest(router, makeRequestWithAuthHeader("GET", "/", "key-nonexisting")).Code)
	assert.Equal(t, http.StatusForbidden, sendRequest(router, makeRequestWithAuthHeader("GET", "/http-bin/get", "key-nonexisting")).Code)
	assert.Equal(t, http.StatusForbidden, sendRequest(router, makeRequestWithAuthHeader("GET", "/non-existing/file", "key-nonexisting")).Code)

	// Create request with correct authorisation data
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithAuthHeader("GET", "/", "key1")).Code)
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequestWithAuthHeader("GET", "/http-bin/get", "key1")).Code)
	assert.Equal(t, http.StatusNotFound, sendRequest(router, makeRequestWithAuthHeader("GET", "/non-existing/file", "key1")).Code)
}

func Test_server_publicURLs(t *testing.T) {
	router := getTestGateway(
		turnstile.Config{
			Proxies:    getProxies(),
			PublicURLs: []string{"/healthz", "/public-dir", "/dir/public-file.json"}},
		getAuthorisers()).getRouter()
	// test that urls not in public urls are still protected
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/")).Code)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(router, makeRequest("GET", "/http-bin/get")).Code)
	// test that public dir is exposed
	assert.Equal(t, http.StatusOK, sendRequest(router, makeRequest("GET", "/healthz")).Code)
	assert.Equal(t, http.StatusNotFound, sendRequest(router, makeRequest("GET", "/public-dir/something-else")).Code)
	assert.Equal(t, http.StatusNotFound, sendRequest(router, makeRequest("GET", "/dir/public-file.json")).Code)
}

func TestServer_CustomRouter_public(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies(), PublicURLs: []string{"/"}}, getAuthorisers())
	// set custom router
	server.AddRoutes(func(router *mux.Router) {
		router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("root of the custom router"))
		})
		router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("healthz handler of the custom router"))
		})
		router.HandleFunc("/http-bin/get", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("overridden handler for proxyHandler"))
		})
	})
	// Test that root is overridden - it is called and returns 111 as defined
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequest("GET", "/")).Code)
	// Test that healthz is overridden as well
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequest("GET", "/healthz")).Code)
	// Test that proxyHandler is overridden as well
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequest("GET", "/http-bin/get")).Code)
	// Test that non-existing route is passed downstream
	assert.Equal(t, 404, sendRequest(server.getRouter(), makeRequest("GET", "/non-existing")).Code)
}

func TestServer_CustomRouter_protected(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers())
	// set custom router
	server.AddRoutes(func(router *mux.Router) {
		router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("root of the custom router"))
		})
		router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("healthz handler of the custom router"))
		})
		router.HandleFunc("/http-bin/get", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("overridden handler for proxyHandler"))
		})
	})
	// Test that root is overridden - it is called and returns 111 as defined
	assert.Equal(t, http.StatusUnauthorized, sendRequest(server.getRouter(), makeRequest("GET", "/")).Code)
	// Test that healthz is overridden as well
	assert.Equal(t, http.StatusUnauthorized, sendRequest(server.getRouter(), makeRequest("GET", "/healthz")).Code)
	// Test that proxyHandler is overridden as well
	assert.Equal(t, http.StatusUnauthorized, sendRequest(server.getRouter(), makeRequest("GET", "/http-bin/get")).Code)
	// Test that non-existing route is passed downstream
	assert.Equal(t, http.StatusUnauthorized, sendRequest(server.getRouter(), makeRequest("GET", "/non-existing")).Code)
}

func TestServer_CustomRouter_forbidden(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers())
	// set custom router
	server.AddRoutes(func(router *mux.Router) {
		router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("root of the custom router"))
		})
		router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("healthz handler of the custom router"))
		})
		router.HandleFunc("/http-bin/get", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("overridden handler for proxyHandler"))
		})
	})
	// Test that root is overridden - it is called and returns 111 as defined
	assert.Equal(t, http.StatusForbidden, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/", "key7")).Code)
	// Test that healthz is overridden as well
	assert.Equal(t, http.StatusForbidden, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/healthz", "key7")).Code)
	// Test that proxyHandler is overridden as well
	assert.Equal(t, http.StatusForbidden, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/http-bin/get", "key7")).Code)
	// Test that non-existing route is passed downstream
	assert.Equal(t, http.StatusForbidden, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/non-existing", "key7")).Code)
}

func TestServer_CustomRouter_authorized(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers())
	// set custom router
	server.AddRoutes(func(router *mux.Router) {
		router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("root of the custom router"))
		})
		router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("healthz handler of the custom router"))
		})
		router.HandleFunc("/http-bin/get", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(111)
			w.Write([]byte("overridden handler for proxyHandler"))
		})
	})
	// Test that root is overridden - it is called and returns 111 as defined
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/", "key1")).Code)
	// Test that healthz is overridden as well
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/healthz", "key1")).Code)
	// Test that proxyHandler is overridden as well
	assert.Equal(t, 111, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/http-bin/get", "key1")).Code)
	// Test that non-existing route is passed downstream
	assert.Equal(t, http.StatusNotFound, sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/non-existing", "key1")).Code)
}

func TestServer_CustomHandlers(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies(), PublicURLs: []string{"/public"}}, getAuthorisers())
	server.Handle401(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("custom 401 handler"))
	})
	server.Handle403(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("custom 403 handler"))
	})
	server.Handle404(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("custom 404 handler"))
	})
	assert.Equal(t, "custom 401 handler", sendRequest(server.getRouter(), makeRequest("GET", "/")).Body.String())
	assert.Equal(t, "custom 403 handler", sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/", "non-existing")).Body.String())
	assert.Equal(t, "custom 404 handler", sendRequest(server.getRouter(), makeRequest("GET", "/public")).Body.String())
	// test shutter handler
	server.shutterHandler = func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("custom shutter handler"))
	}
	assert.Equal(t, "custom shutter handler", sendRequest(server.getRouter(), makeRequest("GET", "/")).Body.String())
	assert.Equal(t, "custom shutter handler", sendRequest(server.getRouter(), makeRequestWithAuthHeader("GET", "/", "non-existing")).Body.String())
	assert.Equal(t, "custom shutter handler", sendRequest(server.getRouter(), makeRequest("GET", "/public")).Body.String())

}

func Test_server_AddPublicURL(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies()}, getAuthorisers())
	assert.Len(t, server.conf.PublicURLs, 0)
	assert.False(t, server.isPublicURL("/test-url/test"))
	server.AddPublicURL("/test-url")
	assert.Len(t, server.conf.PublicURLs, 1)
	assert.Equal(t, "/test-url", server.conf.PublicURLs[0])
	assert.True(t, server.isPublicURL("/test-url/test"))
	server.AddPublicURL("/test-url-2")
	assert.Len(t, server.conf.PublicURLs, 2)
	assert.Equal(t, "/test-url-2", server.conf.PublicURLs[1])
}

func Test_server_isPublicDomain(t *testing.T) {
	gw := server{
		conf: turnstile.Config{PublicDomains: []string{
			"domain.example.com",
			"domain2",
			"*.example2.com",
			"*example3.com",
			"example4?.com",
		}},
	}
	assert.True(t, gw.isPublicDomain("domain.example.com"))
	assert.False(t, gw.isPublicDomain("domainXexample.com"))
	assert.True(t, gw.isPublicDomain("domain2"))
	assert.False(t, gw.isPublicDomain("someother.domain.example.com"))
	assert.False(t, gw.isPublicDomain("localhost"))

	assert.True(t, gw.isPublicDomain("api.example2.com"))
	assert.True(t, gw.isPublicDomain("v1.api.example2.com"))
	assert.False(t, gw.isPublicDomain("example2.com"))

	assert.True(t, gw.isPublicDomain("api.example3.com"))
	assert.True(t, gw.isPublicDomain("v1.api.example3.com"))
	assert.True(t, gw.isPublicDomain("example3.com"))

	assert.True(t, gw.isPublicDomain("example4b.com"))
	assert.True(t, gw.isPublicDomain("example4b.com"))
	assert.False(t, gw.isPublicDomain("example4.com"))
}

func Test_server_AddAuthoriser(t *testing.T) {
	server := getTestGateway(turnstile.Config{Proxies: getProxies()}, nil)
	assert.Equal(t, http.StatusUnauthorized, sendRequest(server.getRouter(), makeRequest("GET", "/")).Code)
	server.AddAuthoriser(&mockAuthoriser{respondAuthorised: true})
	assert.Equal(t, http.StatusOK, sendRequest(server.getRouter(), makeRequest("GET", "/")).Code)
}

func Test_server_startGRPC(t *testing.T) {
	srv := server{
		conf: turnstile.Config{
			GRPCConfig: &turnstile.GrpcConfig{
				Listener:      ":0",
				Targets:       nil,
				EnableGrpcWeb: false,
			},
		},
	}
	// test without enabled grpc web
	assert.NoError(t, srv.startGRPC())
	assert.Nil(t, srv.wrappedGrpcServer)
	assert.True(t, srv.grpcServer.started)

	// test with enabled grpc server
	srv = server{
		conf: turnstile.Config{
			GRPCConfig: &turnstile.GrpcConfig{
				Listener:      ":0",
				Targets:       nil,
				EnableGrpcWeb: true,
			},
		},
	}
	assert.NoError(t, srv.startGRPC())
	assert.NotNil(t, srv.wrappedGrpcServer)
	assert.True(t, srv.grpcServer.started)

	// test grpc server init error
	srv = server{
		conf: turnstile.Config{
			GRPCConfig: &turnstile.GrpcConfig{
				Listener:      ":NONSENSE",
				EnableGrpcWeb: false,
			},
		},
	}
	err := srv.startGRPC()
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "error when initializing grpc proxy")
	assert.False(t, srv.grpcServer.started)

	// test grpc server start error
	srv = server{
		conf: turnstile.Config{
			GRPCConfig: &turnstile.GrpcConfig{
				Listener:      ":0",
				Targets:       nil,
				EnableGrpcWeb: true,
			},
		},
	}
	srv.grpcServer.started = true // this is artificial
	err = srv.startGRPC()
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "server has already started")
	assert.NotNil(t, srv.grpcServer)

	// test option not to start grpc server - for grpc web only option
	srv = server{
		conf: turnstile.Config{
			GRPCConfig: &turnstile.GrpcConfig{
				Listener:            ":0",
				Targets:             nil,
				EnableGrpcWeb:       true,
				DisableGrpcListener: true,
			},
		},
	}
	assert.NoError(t, srv.startGRPC())
	assert.NotNil(t, srv.wrappedGrpcServer)
	assert.False(t, srv.grpcServer.started)
}

func Test_server_StartStopTCP(t *testing.T) {
	srv := server{
		conf: turnstile.Config{
			Proxies: turnstile.ProxyList{
				&turnstile.TCPProxy{},
			},
		},
	}
	// test without enabled grpc web
	assert.NoError(t, srv.StartTCP())
	assert.True(t, srv.tcpServer.started)

}

func Test_server_GetHTTPProxy(t *testing.T) {
	proxy1 := &turnstile.HTTPProxy{Domain: "domain1", Path: "/path1", ID: "proxy1"}
	proxy2 := &turnstile.HTTPProxy{Domain: "domain2", Path: "/path2", ID: "proxy2"}
	srv := server{
		conf: turnstile.Config{
			Proxies: turnstile.ProxyList{
				proxy1,
				&turnstile.HTTPProxy{Domain: "domain2", Path: "/path1"},
				&turnstile.HTTPProxy{Domain: "", Path: "/path1"},
				&turnstile.HTTPProxy{Domain: "domain2", Path: ""},
				&turnstile.HTTPProxy{Domain: "", Path: ""},
				proxy2,
			},
		},
	}
	assert.Same(t, proxy2, srv.GetHTTPProxy("proxy2"))
}

func Test_server_GetTCPProxy(t *testing.T) {
	proxy1 := &turnstile.TCPProxy{Target: "target1", ID: "proxy1"}
	srv := server{
		conf: turnstile.Config{
			Proxies: turnstile.ProxyList{
				&turnstile.HTTPProxy{Domain: "domain2", Path: "/path1"},
				&turnstile.HTTPProxy{Domain: "", Path: "/path1"},
				&turnstile.HTTPProxy{Domain: "domain2", Path: ""},
				&turnstile.HTTPProxy{Domain: "", Path: ""},
				proxy1,
			},
		},
	}
	assert.Same(t, proxy1, srv.GetTCPProxy("proxy1"))
}
