package server

import (
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/turnstile/authoriser"
	"google.golang.org/grpc"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_authoriser(t *testing.T) {
	tests := []struct {
		name                 string
		authorisers          []authoriser.Authoriser
		disableAuthorizers   bool
		handlerCalled        bool
		expectedResponseCode int
	}{
		{
			name:                 "test-none",
			authorisers:          nil,
			handlerCalled:        false,
			expectedResponseCode: http.StatusUnauthorized,
		},
		{
			name:                 "test-none",
			authorisers:          nil,
			disableAuthorizers:   true,
			handlerCalled:        true,
			expectedResponseCode: http.StatusOK,
		},
		{
			name: "test-pass",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondAuthorised: true},
			},
			handlerCalled:        true,
			expectedResponseCode: http.StatusOK,
		},
		{
			name: "test-block",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondAuthorised: false},
			},
			handlerCalled:        false,
			expectedResponseCode: http.StatusForbidden,
		},
		{
			name: "test-pass-multi",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondAuthorised: false},
				&mockAuthoriser{respondAuthorised: true},
			},
			handlerCalled:        true,
			expectedResponseCode: http.StatusOK,
		},
		{
			name: "test-block-multi",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondAuthorised: false},
				&mockAuthoriser{respondAuthorised: false},
			},
			handlerCalled:        false,
			expectedResponseCode: http.StatusForbidden,
		},
		{
			name: "test-err",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondErr: true, respondAuthorised: true},
			},
			handlerCalled:        false,
			expectedResponseCode: http.StatusUnauthorized,
		},
		{
			name: "test-err-multi",
			authorisers: []authoriser.Authoriser{
				&mockAuthoriser{respondAuthorised: true},
				&mockAuthoriser{respondErr: true, respondAuthorised: true},
			},
			handlerCalled:        false,
			expectedResponseCode: http.StatusUnauthorized,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler := &mockHandler{}
			gw := server{
				handler401:         respondUnauthorized,
				handler403:         respondForbidden,
				disableAuthorizers: tt.disableAuthorizers,
			}
			gw.auth = tt.authorisers
			response := httptest.NewRecorder()
			request, _ := http.NewRequest("GET", "/", nil)

			gw.authoriser(handler).ServeHTTP(response, request)
			// test that the handler was called
			assert.Equal(t, tt.handlerCalled, handler.called)
			// test that the response has expected properties
			assert.Equal(t, tt.expectedResponseCode, response.Code)
		})
	}
}

func Test_server_grpcWebHandler(t *testing.T) {
	// test without grpc
	gw := server{}
	handler := &mockHandler{}
	response := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)
	gw.grpcWebHandler(handler).ServeHTTP(response, request)
	assert.True(t, handler.called)
	// test with grpc, not grpc request
	gw = server{wrappedGrpcServer: grpcweb.WrapServer(grpc.NewServer())}
	handler = &mockHandler{}
	response = httptest.NewRecorder()
	request, _ = http.NewRequest("GET", "/", nil)
	gw.grpcWebHandler(handler).ServeHTTP(response, request)
	assert.True(t, handler.called)

	// test mock grpc request
	handler = &mockHandler{}
	response = httptest.NewRecorder()
	request = httptest.NewRequest("POST", "/", nil)
	request.Header.Add("content-type", "application/grpc-web")
	gw.grpcWebHandler(handler).ServeHTTP(response, request)
	assert.False(t, handler.called)
}
