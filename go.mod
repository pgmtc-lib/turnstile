module gitlab.com/pgmtc-lib/turnstile

go 1.17

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/apex/log v1.9.0
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/improbable-eng/grpc-web v0.12.0
	github.com/mwitkow/grpc-proxy v0.0.0-20181017164139-0f1106ef9c76
	github.com/pires/go-proxyproto v0.6.1
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.6.1
	gitlab.com/pgmtc-lib/commons v0.0.0-20220823194921-3dd7144c5d04
	golang.org/x/crypto v0.0.0-20200204104054-c9f3fb736b72
	google.golang.org/grpc v1.28.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
