package turnstile

import (
	"github.com/rs/cors"
	"net/http"
)

// Config is used by the server to set various internals
type Config struct {
	//Proxies       []HTTPProxy // List of domain proxy definitions
	//TCPProxies    []TCPProxy  // list of ports to be forwarded to targets
	Proxies          ProxyList
	ListenerHTTPS    string        // HTTPS listener in unix socket format. Example = :8443
	ListenerHTTP     string        // HTTP listener in unix socket format. Example = :8080
	PublicURLs       []string      // List of URLs excluded from authorisation mechanism.
	PublicDomains    []string      // List of domains excluded from authorisation mechanism, accepts file-like matching (*, ?)
	CompressResponse bool          // Should all response be gzipped
	CORSOptions      *cors.Options // CORS configuration for turnstile
	GRPCConfig       *GrpcConfig   // GRPC configuration
	Handler          http.Handler  // custom handler
}

// GrpcConfig is a grpc server config struct
type GrpcConfig struct {
	CertProvider        CertProvider
	Listener            string
	Targets             map[string]string
	EnableGrpcWeb       bool // if set to true, grpc is wrapped to grpc-web. DisableGrpcListener can be true at the same time
	DisableGrpcListener bool // if set to true, standalone grpc server is disabled
}
