package turnstile

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProxyList_HTTPProxies(t *testing.T) {
	proxy1 := HTTPProxy{Target: "http1"}
	in := ProxyList{
		&proxy1,
		&TCPProxy{Target: "tcp1"},
		&HTTPProxy{Target: "http2"},
		&TCPProxy{Target: "tcp2"},
		&HTTPProxy{Target: "http3"},
	}
	expected := HTTPProxyList{
		&proxy1,
		&HTTPProxy{Target: "http2"},
		&HTTPProxy{Target: "http3"},
	}
	assert.Equal(t, expected, in.HTTPProxies())
	assert.Same(t, in[0], expected[0])
	proxy1.Target = "changeme" // try to change something in the original and test if used pointer
	assert.Equal(t, expected, in.HTTPProxies())
}

func TestProxyList_TCPProxies(t *testing.T) {
	in := ProxyList{
		&HTTPProxy{Target: "http1"},
		&TCPProxy{Target: "tcp1"},
		&HTTPProxy{Target: "http2"},
		&TCPProxy{Target: "tcp2"},
		&HTTPProxy{Target: "http3"},
	}
	expected := TCPProxyList{
		&TCPProxy{Target: "tcp1"},
		&TCPProxy{Target: "tcp2"},
	}
	assert.Equal(t, expected, in.TCPProxies())
}

func TestHTTPProxyList_Find(t *testing.T) {
	pIn := ProxyList{
		&HTTPProxy{Domain: "domain1", Path: "/path1", ID: "proxy1"},
		&HTTPProxy{Domain: "domain2", Path: "/path1", ID: "proxy2"},
		&HTTPProxy{Domain: "domain2", Path: "/path2", ID: "proxy2"}, // deliberately same id
		&HTTPProxy{Domain: "", Path: "/path1"},
		&HTTPProxy{Domain: "domain2", Path: ""},
		&HTTPProxy{Domain: "", Path: ""},
	}

	in := pIn.HTTPProxies()
	assert.Same(t, pIn[0], in.Find("proxy1"))
	assert.Same(t, pIn[1], in.Find("proxy2"))
	assert.Nil(t, in.Find(""))
	assert.Nil(t, in.Find("proxy3"))

}

func TestTCPProxyList_Find(t *testing.T) {
	tcpProxy1 := &TCPProxy{ID: "proxy1", Target: "target1"}
	pIn := ProxyList{
		&HTTPProxy{Target: "http1"},
		&TCPProxy{Target: "tcp1"},
		&HTTPProxy{Target: "http2"},
		&TCPProxy{Target: "tcp2"},
		tcpProxy1,
		&HTTPProxy{Target: "http3"},
	}
	in := pIn.TCPProxies()
	assert.Same(t, pIn[4], in.Find("proxy1"))
	assert.Nil(t, in.Find(""))
	assert.Nil(t, in.Find("proxy2"))

}
