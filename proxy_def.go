package turnstile

import (
	"github.com/rs/cors"
	"gitlab.com/pgmtc-lib/turnstile/authoriser"
	"net/http"
)

// Proxy represents a proxy (either HTTPProxy or TCPProxy)
type Proxy interface {
	dummy()
}

// ProxyList represents list of proxies
type ProxyList []Proxy

// HTTPProxies return list of http proxies
func (l ProxyList) HTTPProxies() (result HTTPProxyList) {
	for _, p := range l {
		p := p
		switch proxy := p.(type) {
		case *HTTPProxy:
			result = append(result, proxy)
		}
	}
	return
}

// TCPProxies returns list of TCP proxies
func (l ProxyList) TCPProxies() (result TCPProxyList) {
	for _, p := range l {
		switch proxy := p.(type) {
		case *TCPProxy:
			result = append(result, proxy)
		}
	}
	return
}

// HTTPProxy is a VO for path proxy
type HTTPProxy struct {
	ID               string                // ID can be set if you want to use turnstile.GetHTTPProxy functionality
	Domain           string                // Handler domain which should be pointing to downstream location. Example = /http-bin
	Path             string                // Handler path which should be pointing to downstream location. Example = /http-bin
	StripPath        bool                  // If set to true, the original path is stripped before sending to the target
	Target           string                // Downstream location target. Example = https://http-bin.org/get
	Authoriser       authoriser.Authoriser // List of authorizers for this specific transparent proxy
	LoginProvider    *authoriser.LoginProvider
	CORSConfig       *cors.Options
	Transport        http.RoundTripper
	Handler403x      http.HandlerFunc                                            // Possible to override 401 handler
	Handler401x      http.HandlerFunc                                            // possible to override 403 handler
	ShutterHandler   http.HandlerFunc                                            // set this handler and every request will be responded by it
	InterceptHandler func(w http.ResponseWriter, r *http.Request) (claimed bool) // InterceptHandler is called before proxy handler (after shutter handler). Use this for handling login/logout, etc. Return claimed=true if you want to stop turnstile from further processing
	RequestModifiers []RequestModifier                                           // RequestModifiers are applied to the outgoing requests to the remote target
}

func (p *HTTPProxy) dummy() {}

// HTTPProxyList represents list of http proxies
type HTTPProxyList []*HTTPProxy

// Find goes through the list of proxies and finds one with the same id. If multiple, returns first
// ID of proxy must not be empty to be returned by this method
func (l HTTPProxyList) Find(id string) (result *HTTPProxy) {
	if id == "" {
		return
	}
	for _, item := range l {
		if item.ID == id {
			return item
		}
	}
	return
}

// TCPProxy hold information about the TCP proxy
type TCPProxy struct {
	ID         string                    // ID can be set if you want to use turnstile.GetTCPProxy functionality
	Listener   string                    // where to listen
	Target     string                    // where to point
	Authoriser authoriser.ConnAuthoriser // optional authoriser of the connection
}

func (p *TCPProxy) dummy() {}

// TCPProxyList represents list of TCP proxies
type TCPProxyList []*TCPProxy

// Find finds tcp proxy in the list
func (l TCPProxyList) Find(id string) (result *TCPProxy) {
	if id == "" {
		return
	}
	for _, item := range l {
		if item.ID == id {
			return item
		}
	}
	return
}

// RequestModifier is a generic request modifier
type RequestModifier interface {
	Apply(req *http.Request)
}
