package main

import (
	"flag"
	apexlog "github.com/apex/log"
	"gitlab.com/pgmtc-lib/commons/run"
	"gitlab.com/pgmtc-lib/turnstile"
	"gitlab.com/pgmtc-lib/turnstile/server"
	"gitlab.com/pgmtc-lib/turnstile/server/certprovider"
	"golang.org/x/crypto/acme/autocert"
	"log"
	"os"
)

var certProviderParam string

func init() {
	flag.StringVar(&certProviderParam, "cp", "", "cert provider (selfsigned, letsencrypt, provided)")
	flag.Parse()
}

func main() {
	apexlog.SetLevel(apexlog.DebugLevel)
	config := turnstile.Config{
		ListenerHTTPS: ":443",
		PublicURLs:    []string{"/"},
	}
	srv := server.New(config, getCertProvider())
	run.IfErrorThenExit(srv.Start(), "error when starting turnstile")
	select {}
}

func getCertProvider() turnstile.CertProvider {
	if certProviderParam == "" {
		log.Fatalf("please provide cert provider")
	}
	switch certProviderParam {
	case "letsencrypt":
		return letsEncryptCertProvider()
	case "selfsigned":
		return selfSignedCertProvider()
	case "provided":
		return envCertProvider()
	case "files":
		return fileCertProvider()
	}
	log.Fatalf("unknown certprovider %s", certProviderParam)
	return nil
}

func fileCertProvider() turnstile.CertProvider {
	return certprovider.FromFiles("cert.pem", "key.pem")
}

func envCertProvider() turnstile.CertProvider {
	return certprovider.Provide([]byte(os.Getenv("CERT")), []byte(os.Getenv("KEY")))
}

func letsEncryptCertProvider() turnstile.CertProvider {
	certManager := &autocert.Manager{
		Prompt: autocert.AcceptTOS,
		Cache:  autocert.DirCache("/tmp/"),
	}
	certprovider.LetsEncryptCertProvider(certManager)
	return certprovider.LetsEncryptCertProvider(certManager)
}

func selfSignedCertProvider() turnstile.CertProvider {
	return certprovider.GenerateSelfSigned("turnstile", []string{})
}
