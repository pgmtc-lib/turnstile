package authoriser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncrypt2(t *testing.T) {
	key := randomString(32)
	toEncrypt := randomString(100)
	encoded, err := Encrypt(key, toEncrypt)
	if !assert.NoError(t, err) {
		return
	}
	decrypted, err := Decrypt(key, encoded)
	if !assert.NoError(t, err) {
		return
	}
	if decrypted != toEncrypt {
		t.Errorf("decrypted = %q, expected %q", decrypted, toEncrypt)
	}
}

func Benchmark_Crypt(b *testing.B) {
	key := randomString(32)
	toEncrypt := randomString(100)
	for i := 0; i < b.N; i++ {
		encoded, _ := Encrypt(key, toEncrypt)
		decrypted, _ := Decrypt(key, encoded)
		if decrypted != toEncrypt {
			b.Errorf("decrypted = %q, expected %q", decrypted, toEncrypt)
		}
	}
}
