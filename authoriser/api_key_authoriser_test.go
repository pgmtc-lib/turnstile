package authoriser

import (
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

func TestAPIKeyAuthoriser_Authorise(t *testing.T) {
	// Test constructor
	cstr := NewAPIKeyAuthoriser("key-header", "client-id", map[string]string{}).(*APIKeyAuthoriser)
	strc := &APIKeyAuthoriser{inHeaderName: "key-header", outHeaderName: "client-id", keyToIDMap: map[string]string{}}
	assert.Equal(t, cstr, strc)

	// Test the rest
	auth := APIKeyAuthoriser{inHeaderName: "key-header"}
	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("key-header", "key2")

	// Test with empty authorizer
	authorized, err := auth.Authorise(req)
	assert.NoError(t, err)
	assert.False(t, authorized)

	// add something
	auth = APIKeyAuthoriser{
		inHeaderName: "key-header",
		keyToIDMap: map[string]string{
			"key1": "user1",
			"key2": "user2",
		},
	}
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.True(t, authorized)

	// test non existing key
	req.Header.Set("key-header", "non-existing")
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.False(t, authorized)

}

func TestAPIKeyAuthoriser_stampRequest(t *testing.T) {
	auth := APIKeyAuthoriser{}
	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	auth.stampRequest(req, "test-client")
	assert.Equal(t, "test-client", req.Header.Get("client-id"))
}
