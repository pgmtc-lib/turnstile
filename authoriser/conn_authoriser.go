package authoriser

import (
	"net"
)

// ConnAuthoriser is a signature for TCP authoriser
type ConnAuthoriser func(conn net.Conn) (authorized bool)

// IPWhiteList returns IP white list authoriser
func IPWhiteList(cidrList ...string) (result ConnAuthoriser) {
	ips, ipNets := parseCIDRs(cidrList)
	result = func(conn net.Conn) (authorized bool) {
		ip, _, err := net.SplitHostPort(conn.RemoteAddr().String())
		if err != nil {
			return false
		}
		return ipIn(ip, ips, ipNets)
	}
	return
}

// IPBlackList returns IP black list authoriser
func IPBlackList(cidrList ...string) (result ConnAuthoriser) {
	ips, ipNets := parseCIDRs(cidrList)
	result = func(conn net.Conn) (authorized bool) {
		ip, _, err := net.SplitHostPort(conn.RemoteAddr().String())
		if err != nil {
			return false
		}
		return !ipIn(ip, ips, ipNets)
	}
	return
}

// ipIn returns whether the address matches or is in the provided networks
func ipIn(remoteAddr string, ips []net.IP, ipNets []net.IPNet) bool {
	remoteIP := net.ParseIP(remoteAddr)
	// test ip addresses
	for _, ip := range ips {
		if ip.Equal(remoteIP) {
			return true
		}
	}
	for _, ipNet := range ipNets {
		if ipNet.Contains(remoteIP) {
			return true
		}
	}
	return false
}

// parseCIDRs returns list of IPs and IPNets for the provided list of CIDRs
func parseCIDRs(cidrList []string) (ips []net.IP, ipNets []net.IPNet) {
	for _, cidr := range cidrList {
		// try ip
		ip, ipNet, err := net.ParseCIDR(cidr)
		if err != nil {
			// try parse IP
			ip = net.ParseIP(cidr)
		}
		ips = append(ips, ip)
		if ipNet != nil {
			ipNets = append(ipNets, *ipNet)
		}
	}
	return
}
