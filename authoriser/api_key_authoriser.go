package authoriser

import (
	"fmt"
	"net/http"
)

// APIKeyAuthoriser is an example authoriser which authenticates against the API key
type APIKeyAuthoriser struct {
	inHeaderName  string
	outHeaderName string
	keyToIDMap    map[string]string
}

// PreAuthorise does not provide any login page
func (a *APIKeyAuthoriser) PreAuthorise(w http.ResponseWriter, r *http.Request) (claimed bool) {
	return false
}

// Authorise returns true if the key is valid
func (a *APIKeyAuthoriser) Authorise(r *http.Request) (authorised bool, resultErr error) {
	headerKey := r.Header.Get(a.inHeaderName)
	if headerKey == "" {
		authorised = false
		resultErr = fmt.Errorf("missing header")
		return
	}
	var clientID string
	clientID, authorised = a.keyToIDMap[headerKey]
	if authorised && clientID != "" {
		a.stampRequest(r, clientID)
	}
	return
}

// PostAuthorise processes authorization result
func (a *APIKeyAuthoriser) PostAuthorise(w http.ResponseWriter, r *http.Request, authorised bool, authoriseError error) (claimed bool) {
	return false
}

func (a *APIKeyAuthoriser) stampRequest(req *http.Request, apiClient string) {
	req.Header.Set("client-id", apiClient)
}

// NewAPIKeyAuthoriser is a constructor
func NewAPIKeyAuthoriser(inHeaderName, outHeaderName string, keyUIDs map[string]string) Authoriser {
	return &APIKeyAuthoriser{
		inHeaderName:  inHeaderName,
		outHeaderName: outHeaderName,
		keyToIDMap:    keyUIDs,
	}
}
