package authoriser

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

type LoginProvider struct {
	Path    string       // for example /auth/
	Handler http.Handler // handler func of the login provider
}

func (l LoginProvider) NormalizedPath() (result string) {
	result = l.Path
	if !strings.HasPrefix(l.Path, "/") {
		result = "/" + result
	}
	if !strings.HasSuffix(l.Path, "/") {
		result = result + "/"
	}
	return result
}

func (l LoginProvider) RedirectToMe(w http.ResponseWriter, r *http.Request) {
	var redirectURL string
	currentOrigin, _ := url.QueryUnescape(strings.TrimSpace(r.URL.Query().Get("redirect")))
	switch {
	case currentOrigin != "":
		redirectURL = fmt.Sprintf("%s?redirect=%s", l.NormalizedPath(), url.QueryEscape(currentOrigin))
	case r.URL.Path != "" && r.URL.Path != "/":
		redirectURL = fmt.Sprintf("%s?redirect=%s", l.NormalizedPath(), url.QueryEscape(r.RequestURI))
	default:
		redirectURL = l.NormalizedPath()
	}
	http.Redirect(w, r, redirectURL, http.StatusTemporaryRedirect)
}
