package authoriser

import (
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

func Test_anyAuthoriser_Authorise(t *testing.T) {
	auth1 := NewAPIKeyAuthoriser("auth", "user", map[string]string{
		"key1": "user1",
		"key2": "user2",
	})
	auth2 := NewAPIKeyAuthoriser("auth2", "user", map[string]string{
		"key3": "user3",
		"key4": "user4",
	})
	auth := UseAny(auth1, auth2)
	// test with no compatible authorisers
	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("unknown-header", "non-existing")
	authorized, err := auth.Authorise(req)
	assert.EqualError(t, err, "missing header") // error from the last authoriser
	assert.False(t, authorized)
	assert.Equal(t, "", req.Header.Get("user"))
	// test with second authoriser - not passing
	req = httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("auth2", "non-existing")
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.False(t, authorized)
	assert.Equal(t, "", req.Header.Get("user"))
	// test with first authoriser - not passing
	req = httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("auth", "non-existing")
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.False(t, authorized)
	assert.Equal(t, "", req.Header.Get("user"))
	// test with second authoriser - passing
	req = httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("auth2", "key3")
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.True(t, authorized)
	assert.Equal(t, "", req.Header.Get("user3"))
	// test with first authoriser - passing
	req = httptest.NewRequest("GET", "http://example.com/foo", nil)
	req.Header.Set("auth", "key1")
	authorized, err = auth.Authorise(req)
	assert.NoError(t, err)
	assert.True(t, authorized)
	assert.Equal(t, "", req.Header.Get("user1"))
}
