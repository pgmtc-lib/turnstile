package authoriser

import (
	"github.com/stretchr/testify/assert"
	"net"
	"testing"
	"time"
)

func TestIPWhiteListAuthoriser(t *testing.T) {
	cidrList := []string{
		"192.168.1.1",
		"10.1.1.1",
		"10.1.1.2/24",
	}
	auth := IPWhiteList(cidrList...)
	assert.True(t, auth(&mockConn{remoteAddr: "192.168.1.1"}))
	assert.True(t, auth(&mockConn{remoteAddr: "10.1.1.1"}))
	assert.True(t, auth(&mockConn{remoteAddr: "10.1.1.2"}))
	assert.True(t, auth(&mockConn{remoteAddr: "10.1.1.15"}))
	assert.False(t, auth(&mockConn{remoteAddr: "192.168.1.2"}))
	assert.False(t, auth(&mockConn{remoteAddr: "10.1.2.1"}))
	assert.False(t, auth(&mockConn{remoteAddr: ""}))
}

func TestIPBlackListAuthoriser(t *testing.T) {
	cidrList := []string{
		"192.168.1.1",
		"10.1.1.1",
		"10.1.1.2/24",
	}
	auth := IPBlackList(cidrList...)
	assert.False(t, auth(&mockConn{remoteAddr: "192.168.1.1"}))
	assert.False(t, auth(&mockConn{remoteAddr: "10.1.1.1"}))
	assert.False(t, auth(&mockConn{remoteAddr: "10.1.1.2"}))
	assert.False(t, auth(&mockConn{remoteAddr: "10.1.1.15"}))
	assert.True(t, auth(&mockConn{remoteAddr: "192.168.1.2"}))
	assert.True(t, auth(&mockConn{remoteAddr: "10.1.2.1"}))
	assert.True(t, auth(&mockConn{remoteAddr: ""}))
}

func Test_ipIn(t *testing.T) {
	cidrList := []string{
		"192.168.1.1",
		"10.1.1.1",
		"10.1.1.2/28",
	}
	ips, ipNets := parseCIDRs(cidrList)
	assert.True(t, ipIn("192.168.1.1", ips, ipNets))
	assert.True(t, ipIn("10.1.1.1", ips, ipNets))
	assert.True(t, ipIn("10.1.1.2", ips, ipNets))
	assert.True(t, ipIn("10.1.1.3", ips, ipNets))
	assert.True(t, ipIn("10.1.1.15", ips, ipNets))
	assert.False(t, ipIn("10.1.1.16", ips, ipNets))
	assert.False(t, ipIn("10.1.2.16", ips, ipNets))
	assert.False(t, ipIn("192.168.1.2", ips, ipNets))
	assert.False(t, ipIn("192.168", ips, ipNets))
	assert.False(t, ipIn("", ips, ipNets))
}

func Test_parseCIDRs(t *testing.T) {
	cidrList := []string{
		"192.168.1.1",
		"10.1.1.1",
		"10.1.1.2/24",
	}
	ips, ipNets := parseCIDRs(cidrList)
	assert.Len(t, ips, 3)
	assert.Len(t, ipNets, 1)

	expectedIPs := []net.IP{
		net.ParseIP("192.168.1.1"),
		net.ParseIP("10.1.1.1"),
		net.ParseIP("10.1.1.2"),
	}
	_, expectedIPNet1, err := net.ParseCIDR("10.1.1.2/24")
	assert.NoError(t, err)

	assert.Equal(t, expectedIPs, ips)
	assert.Equal(t, *expectedIPNet1, ipNets[0])
}

type mockConn struct {
	remoteAddr string
}

func (m mockConn) Read(b []byte) (n int, err error)   { return 0, nil }
func (m mockConn) Write(b []byte) (n int, err error)  { return 0, nil }
func (m mockConn) Close() error                       { return nil }
func (m mockConn) LocalAddr() net.Addr                { return nil }
func (m mockConn) SetDeadline(t time.Time) error      { return nil }
func (m mockConn) SetReadDeadline(t time.Time) error  { return nil }
func (m mockConn) SetWriteDeadline(t time.Time) error { return nil }
func (m mockConn) RemoteAddr() net.Addr {
	return &net.TCPAddr{IP: net.ParseIP(m.remoteAddr), Port: 1234}
}
