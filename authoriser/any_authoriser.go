package authoriser

import "net/http"

// AnyAuthoriser is used for combining two or more authorisers, any of them can authorise
type anyAuthoriser []Authoriser

// PreAuthorise iterates through authorisers login intercept handlers
func (a anyAuthoriser) PreAuthorise(w http.ResponseWriter, r *http.Request) (claimed bool) {
	for _, auth := range a {
		if auth.PreAuthorise(w, r) {
			return true
		}
	}
	return false
}

// Authorise authorises the request
func (a anyAuthoriser) Authorise(r *http.Request) (authorised bool, resultErr error) {
	for _, auth := range a {
		if authorised, resultErr = auth.Authorise(r); resultErr == nil {
			return
		}
	}
	return
}

// PostAuthorise processes result of authorisation
func (a anyAuthoriser) PostAuthorise(w http.ResponseWriter, r *http.Request, authorised bool, authoriseError error) (claimed bool) {
	for _, auth := range a {
		if auth.PostAuthorise(w, r, authorised, authoriseError) {
			return true
		}
	}
	return false
}

// UseAny returns authoriser which authorises if any of the provided authorisers pass
func UseAny(authorisers ...Authoriser) Authoriser {
	return anyAuthoriser(authorisers)
}
