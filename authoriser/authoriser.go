package authoriser

import (
	"net/http"
)

// Authoriser is an interface for custom authoriser implementations
type Authoriser interface {
	PreAuthorise(w http.ResponseWriter, r *http.Request) (claimed bool)
	Authorise(r *http.Request) (authorised bool, resultErr error)
	PostAuthorise(w http.ResponseWriter, r *http.Request, authorised bool, authoriseError error) (claimed bool)
}
