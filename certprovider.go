package turnstile

import "crypto/tls"

// CertProvider is an interface provided to the TLS config respondible for returning certificates
type CertProvider interface {
	Initialize() error
	GetCertificate(*tls.ClientHelloInfo) (*tls.Certificate, error)
}
